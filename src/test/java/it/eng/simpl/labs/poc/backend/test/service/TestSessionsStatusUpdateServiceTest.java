package it.eng.simpl.labs.poc.backend.test.service;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.mockito.ArgumentMatchers.anyList;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.BDDMockito.given;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import it.eng.simpl.labs.poc.entity.TestCaseExecutionEntity;
import it.eng.simpl.labs.poc.entity.TestExecutionEntity;
import it.eng.simpl.labs.poc.entity.TestSuiteExecutionEntity;
import it.eng.simpl.labs.poc.enumeration.ItbResultEnum;
import it.eng.simpl.labs.poc.enumeration.TestStatusEnum;
import it.eng.simpl.labs.poc.model.itb.SessionStatus;
import it.eng.simpl.labs.poc.model.itb.SessionStatusResponse;
import it.eng.simpl.labs.poc.repository.TestCaseExecutionRepository;
import it.eng.simpl.labs.poc.repository.TestExecutionRepository;
import it.eng.simpl.labs.poc.repository.TestSuiteExecutionRepository;
import it.eng.simpl.labs.poc.service.ItbTestSessionsManagementService;
import it.eng.simpl.labs.poc.service.TestSessionsStatusUpdateService;

@ExtendWith(MockitoExtension.class)
class TestSessionsStatusUpdateServiceTest {
	
	private static final String TEST_SESSION = "test-session";
	
	private static final String WRONG_TEST_SESSION = "wromg-test-session";

	@InjectMocks
	private TestSessionsStatusUpdateService testSessionsStatusUpdateService;
	
	@Mock
	private TestCaseExecutionRepository testCaseExecutionRepository;
	
	@Mock
	private ItbTestSessionsManagementService itbTestSessionsManagementService;
	
	@Mock
	private TestSuiteExecutionRepository testSuiteExecutionRepository;

	@Mock
	private TestExecutionRepository testExecutionRepository;
	
	private TestCaseExecutionEntity testCaseExecutionEntity;
	
	private TestExecutionEntity testExecutionEntity;
	
	private SessionStatusResponse sessionStatusResponse;
	
	@BeforeEach
	public void setup(){
		
		testExecutionEntity = new TestExecutionEntity();
		testExecutionEntity.setStopRequestDate(LocalDateTime.now());
		
		testCaseExecutionEntity = new TestCaseExecutionEntity();
		testCaseExecutionEntity.setId(123L);
		testCaseExecutionEntity.setStatus(TestStatusEnum.SUCCESS);
		testCaseExecutionEntity.setSession(TEST_SESSION);
		testCaseExecutionEntity.setStartDate(LocalDateTime.now().minusDays(1));
		testCaseExecutionEntity.setEndDate(LocalDateTime.now().minusDays(1));
		
		TestSuiteExecutionEntity testSuiteExecutionEntity = new TestSuiteExecutionEntity();
		testSuiteExecutionEntity.setTestCaseExecutionList(Arrays.asList(new TestCaseExecutionEntity()));
		
		testSuiteExecutionEntity.setTestExecution(new TestExecutionEntity());
		testSuiteExecutionEntity.getTestExecution().setId(1L);
		
		testCaseExecutionEntity.setTestSuiteExecution(testSuiteExecutionEntity);
				
		sessionStatusResponse = new SessionStatusResponse();
		SessionStatus sessionStatus = new SessionStatus();
		sessionStatus.setSession(TEST_SESSION);
		sessionStatus.setResult(ItbResultEnum.SUCCESS);
		sessionStatusResponse.setSessions(Arrays.asList(sessionStatus));

	}
	
	@DisplayName("Test for TestSessionsStatusUpdateService.updateTestExecutionStatus - Success")
    @Test
    void updateTestExecutionStatusSuccess(){
		
		testCaseExecutionEntity.getTestSuiteExecution().getTestCaseExecutionList().getFirst().setStatus(TestStatusEnum.SUCCESS);
		
		given(testCaseExecutionRepository.findByEndDateIsNull()).willReturn(Arrays.asList(testCaseExecutionEntity));
		
		given(itbTestSessionsManagementService.getSessionStatus(Arrays.asList(TEST_SESSION), Boolean.TRUE, Boolean.TRUE)).willReturn(sessionStatusResponse);

		given(testCaseExecutionRepository.saveAllAndFlush(anyList())).willReturn(null);
		
		given(testSuiteExecutionRepository.saveAllAndFlush(anyList())).willReturn(null);
				
		testSessionsStatusUpdateService.updateTestExecutionStatus();
		
		testCaseExecutionEntity.setStartDate(null);
		
		testSessionsStatusUpdateService.updateTestExecutionStatus();
		
		testCaseExecutionEntity.setEndDate(null);
		
	    assertDoesNotThrow(() -> {
			testSessionsStatusUpdateService.updateTestExecutionStatus();
	    });	

	}
	
	@DisplayName("Test for TestSessionsStatusUpdateService.updateTestExecutionStatus - Wrong Session")
    @Test
    void updateTestExecutionStatusWrongSession(){
		
		testCaseExecutionEntity.getTestSuiteExecution().getTestCaseExecutionList().getFirst().setStatus(TestStatusEnum.SUCCESS);
		
		testCaseExecutionEntity.setSession(WRONG_TEST_SESSION);
		
		given(testCaseExecutionRepository.findByEndDateIsNull()).willReturn(Arrays.asList(testCaseExecutionEntity));
		
		given(itbTestSessionsManagementService.getSessionStatus(Arrays.asList(WRONG_TEST_SESSION), Boolean.TRUE, Boolean.TRUE)).willReturn(sessionStatusResponse);

		given(testCaseExecutionRepository.saveAllAndFlush(anyList())).willReturn(null);
		
		given(testExecutionRepository.findById(anyLong())).willReturn(Optional.of(testExecutionEntity));
		
		given(testSuiteExecutionRepository.saveAllAndFlush(anyList())).willReturn(null);
				
	    assertDoesNotThrow(() -> {
			testSessionsStatusUpdateService.updateTestExecutionStatus();
	    });	
	}
	
	@DisplayName("Test for TestSessionsStatusUpdateService.updateTestExecutionStatus - Status Failure")
    @Test
    void updateTestExecutionStatusFailure(){
		
		testCaseExecutionEntity.getTestSuiteExecution().getTestCaseExecutionList().getFirst().setStatus(TestStatusEnum.FAILURE);

		given(testCaseExecutionRepository.findByEndDateIsNull()).willReturn(Arrays.asList(testCaseExecutionEntity));
		
		given(itbTestSessionsManagementService.getSessionStatus(Arrays.asList(TEST_SESSION), Boolean.TRUE, Boolean.TRUE)).willReturn(sessionStatusResponse);

		given(testCaseExecutionRepository.saveAllAndFlush(anyList())).willReturn(null);
		
		given(testSuiteExecutionRepository.saveAllAndFlush(anyList())).willReturn(null);
						
	    assertDoesNotThrow(() -> {
			testSessionsStatusUpdateService.updateTestExecutionStatus();
	    });	
	}
	
	@DisplayName("Test for TestSessionsStatusUpdateService.updateTestExecutionStatus - Status Undefined")
    @Test
    void updateTestExecutionStatusUndefined(){
		
		testCaseExecutionEntity.getTestSuiteExecution().getTestCaseExecutionList().getFirst().setStatus(TestStatusEnum.UNDEFINED);

		given(testCaseExecutionRepository.findByEndDateIsNull()).willReturn(Arrays.asList(testCaseExecutionEntity));
		
		given(itbTestSessionsManagementService.getSessionStatus(Arrays.asList(TEST_SESSION), Boolean.TRUE, Boolean.TRUE)).willReturn(sessionStatusResponse);

		given(testCaseExecutionRepository.saveAllAndFlush(anyList())).willReturn(null);
		
		given(testSuiteExecutionRepository.saveAllAndFlush(anyList())).willReturn(null);
						
	    assertDoesNotThrow(() -> {
			testSessionsStatusUpdateService.updateTestExecutionStatus();
	    });	
	}
	
	@DisplayName("Test for TestSessionsStatusUpdateService.updateTestExecutionStatus - Start date null")
    @Test
    void updateTestExecutionStartDateNull(){
		
		testCaseExecutionEntity.getTestSuiteExecution().getTestCaseExecutionList().getFirst().setStartDate(null);

		given(testCaseExecutionRepository.findByEndDateIsNull()).willReturn(Arrays.asList(testCaseExecutionEntity));
		
		given(itbTestSessionsManagementService.getSessionStatus(Arrays.asList(TEST_SESSION), Boolean.TRUE, Boolean.TRUE)).willReturn(sessionStatusResponse);

		given(testCaseExecutionRepository.saveAllAndFlush(anyList())).willReturn(null);
		
		given(testSuiteExecutionRepository.saveAllAndFlush(anyList())).willReturn(null);
						
	    assertDoesNotThrow(() -> {
			testSessionsStatusUpdateService.updateTestExecutionStatus();
	    });	
	}
	
	@DisplayName("Test for TestSessionsStatusUpdateService.updateTestExecutionStatus - End date null")
    @Test
    void updateTestExecutionEndDateNull(){
		
		testCaseExecutionEntity.getTestSuiteExecution().getTestCaseExecutionList().getFirst().setEndDate(null);

		given(testCaseExecutionRepository.findByEndDateIsNull()).willReturn(Arrays.asList(testCaseExecutionEntity));
		
		given(itbTestSessionsManagementService.getSessionStatus(Arrays.asList(TEST_SESSION), Boolean.TRUE, Boolean.TRUE)).willReturn(sessionStatusResponse);

		given(testCaseExecutionRepository.saveAllAndFlush(anyList())).willReturn(null);
		
		given(testSuiteExecutionRepository.saveAllAndFlush(anyList())).willReturn(null);
		
	    assertDoesNotThrow(() -> {
			testSessionsStatusUpdateService.updateTestExecutionStatus();
	    });	
				
	}
	
	@DisplayName("Test for TestSessionsStatusUpdateService.updateTestExecutionStatus - End date null")
    @Test
    void updateTestExecutionStatusUndefinedEndDateNull(){
		
		given(testCaseExecutionRepository.findByEndDateIsNull()).willReturn(null);
				
	    assertDoesNotThrow(() -> {
			testSessionsStatusUpdateService.updateTestExecutionStatus();
	    });	
	}
}
