package it.eng.simpl.labs.poc.backend.test.utils;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.when;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.math.BigDecimal;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.PageRequest;
import org.springframework.security.oauth2.jwt.Jwt;

import it.eng.simpl.labs.poc.model.JwtUser;
import it.eng.simpl.labs.poc.model.User;
import it.eng.simpl.labs.poc.utils.Constants;
import it.eng.simpl.labs.poc.utils.GenericUtils;
import it.eng.simpl.labs.poc.utils.ItbConfigurationUtils;
import it.eng.simpl.labs.poc.utils.JwtUtil;

@ExtendWith(MockitoExtension.class)
class UtilsTest {

	@Mock
	private Jwt jwtMock;

	@BeforeEach
	public void setup() {

	}

	@DisplayName("Test for GenericUtils.getPageRequest")
	@Test
	void genericUtilsGetPageRequest() {

		PageRequest pageRequest = GenericUtils.getPageRequest(1, 1, "x");

		assertNotNull(pageRequest);

		pageRequest = GenericUtils.getPageRequest(1, 1, null);

		assertNotNull(pageRequest);
	}

	@DisplayName("Test for GenericUtils.GenericUtils")
	@Test
	void genericUtilsConstructor() throws Exception {

		Constructor<GenericUtils> constructor = GenericUtils.class.getDeclaredConstructor();
		constructor.setAccessible(true);

		assertThrows(InvocationTargetException.class, () -> {
			constructor.newInstance();
		});
	}

	@DisplayName("Test for GenericUtils.getLocalDateTimeFromString")
	@Test
	void genericUtilsGetLocalDateTimeFromString() {

		assertDoesNotThrow(() -> {
			GenericUtils.getLocalDateTimeFromString("2024-10-10T12:12:12Z");
		});
	}

	@DisplayName("Test for GenericUtils.getRandomBigDecimalBetween")
	@Test
	void genericUtilsGetRandomBigDecimalBetween() {

		assertThrows(IllegalArgumentException.class, () -> {
			GenericUtils.getRandomBigDecimalBetween(BigDecimal.TWO, BigDecimal.ONE);
		});

	}

	@DisplayName("Test for ItbConfigurationUtils.ItbConfigurationUtils")
	@Test
	void itbConfigurationUtilsConstructor() throws Exception {

		Constructor<ItbConfigurationUtils> constructor = ItbConfigurationUtils.class.getDeclaredConstructor();
		constructor.setAccessible(true);

		assertThrows(InvocationTargetException.class, () -> {
			constructor.newInstance();
		});
	}

	@DisplayName("Test for JwtUtil.JwtUtil")
	@Test
	void jwtUtilConstructor() throws Exception {

		Constructor<JwtUtil> constructor = JwtUtil.class.getDeclaredConstructor();
		constructor.setAccessible(true);

		assertThrows(InvocationTargetException.class, () -> {
			constructor.newInstance();
		});
	}

	@DisplayName("Test for JwtUtil.createJwtUser")
	@Test
	void jwtUtilcreateJwtUser() {

		when(jwtMock.getClaimAsString("email")).thenReturn("test@example.com");
		when(jwtMock.getClaimAsString("preferred_username")).thenReturn("testuser");
		when(jwtMock.getClaimAsString("name")).thenReturn("Test User");

		JwtUser jwtUser = JwtUtil.createJwtUser(jwtMock);

		User user = jwtUser.getUser();

		assertEquals("test@example.com", user.getEmail());
		assertEquals("testuser", user.getUsername());
		assertEquals("Test User", user.getName());

	}

	@DisplayName("Test for Constants.Constants")
	@Test
	void constantsConstructor() throws Exception {

		Constructor<Constants> constructor = Constants.class.getDeclaredConstructor();
		constructor.setAccessible(true);

		assertThrows(InvocationTargetException.class, () -> {
			constructor.newInstance();
		});
	}

}