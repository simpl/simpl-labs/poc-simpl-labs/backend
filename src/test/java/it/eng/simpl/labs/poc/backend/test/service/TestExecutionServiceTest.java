package it.eng.simpl.labs.poc.backend.test.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyList;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.BDDMockito.given;

import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockedStatic;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import it.eng.simpl.labs.poc.entity.TestCaseExecutionEntity;
import it.eng.simpl.labs.poc.entity.TestExecutionEntity;
import it.eng.simpl.labs.poc.entity.TestSuiteExecutionEntity;
import it.eng.simpl.labs.poc.enumeration.TestStatusEnum;
import it.eng.simpl.labs.poc.mapper.TestCaseMapper;
import it.eng.simpl.labs.poc.mapper.TestExecutionMapper;
import it.eng.simpl.labs.poc.model.TestCase;
import it.eng.simpl.labs.poc.model.TestCaseExecutionLogs;
import it.eng.simpl.labs.poc.model.TestCaseExecutionReport;
import it.eng.simpl.labs.poc.model.TestCaseStep;
import it.eng.simpl.labs.poc.model.TestExecution;
import it.eng.simpl.labs.poc.model.TestSessionStatus;
import it.eng.simpl.labs.poc.repository.TestCaseExecutionRepository;
import it.eng.simpl.labs.poc.repository.TestExecutionRepository;
import it.eng.simpl.labs.poc.service.TestExecutionService;
import it.eng.simpl.labs.poc.utils.ItbConfigurationUtils;

@ExtendWith(MockitoExtension.class)
class TestExecutionServiceTest {
	
	@InjectMocks
	private TestExecutionService testExecutionService;
	
	@Mock
	private TestExecutionRepository testExecutionRepository;
	
	@Mock
	private TestCaseExecutionRepository testCaseExecutionRepository;
	
	@Mock
	private TestExecutionMapper testExecutionMapper;
	
	@Mock
	private TestCaseMapper testCaseMapper;
		
	private TestExecutionEntity testExecutionEntity;
	
	private TestCaseExecutionEntity testCaseExecutionEntity;

	private TestExecution testExecution;
	
	private TestCase testCase;
	
	@BeforeAll
	public static void generalSetup(){
		
        MockedStatic<ItbConfigurationUtils> mockedStatic = Mockito.mockStatic(ItbConfigurationUtils.class);
        mockedStatic.when(() -> ItbConfigurationUtils.getTestCaseStepResult(anyString(), anyString())).thenReturn("");
	}
	
	@BeforeEach
	public void setup(){
		
		testExecutionEntity = new TestExecutionEntity();
		TestSuiteExecutionEntity testSuiteExecutionEntity = new TestSuiteExecutionEntity(); 
		testSuiteExecutionEntity.setTestCaseExecutionList(Arrays.asList(new TestCaseExecutionEntity()));
		testExecutionEntity.setTestSuiteExecutionList(Arrays.asList(testSuiteExecutionEntity));

		testExecution = TestExecution.builder().build();
		
		testCase = TestCase.builder().build();
		TestCaseStep testCaseStep = TestCaseStep.builder().build();
		testCase.setStepsList(Arrays.asList(testCaseStep));
	
		testCaseExecutionEntity = new TestCaseExecutionEntity();
	}
	
	@DisplayName("Test for TestExecutionService.getTestHistory - success")
    @Test
    void getTestHistorySuccess() {
		
		testExecutionEntity.getTestSuiteExecutionList().getFirst().getTestCaseExecutionList().getFirst().setStatus(TestStatusEnum.SUCCESS);
		
		given(testExecutionRepository.findByCustomComponentIdAndStatusInOrderByIdAsc(anyLong(), anyList())).willReturn(Arrays.asList(testExecutionEntity));
		given(testExecutionMapper.createFromEntity(any(TestExecutionEntity.class))).willReturn(testExecution);
		given(testCaseMapper.createFromExecutionEntity(any(TestCaseExecutionEntity.class))).willReturn(testCase);

		List<TestSessionStatus> testSessionStatusList = testExecutionService.getTestHistory(1L);
		
	    assertThat(testSessionStatusList).isNotNull().hasSizeGreaterThan(0);		
			
	}
	
	@DisplayName("Test for TestExecutionService.getTestHistory - failure")
    @Test
    void getTestHistoryError() {
		
		testExecutionEntity.getTestSuiteExecutionList().getFirst().getTestCaseExecutionList().getFirst().setStatus(TestStatusEnum.FAILURE);
		
		given(testExecutionRepository.findByCustomComponentIdAndStatusInOrderByIdAsc(anyLong(), anyList())).willReturn(Arrays.asList(testExecutionEntity));
		given(testExecutionMapper.createFromEntity(any(TestExecutionEntity.class))).willReturn(testExecution);
		given(testCaseMapper.createFromExecutionEntity(any(TestCaseExecutionEntity.class))).willReturn(testCase);

		List<TestSessionStatus> testSessionStatusList = testExecutionService.getTestHistory(1L);
		
	    assertThat(testSessionStatusList).isNotNull().hasSizeGreaterThan(0);		
			
	}
	
	@DisplayName("Test for TestExecutionService.getTestsExecutionStatus - Failure")
    @Test
    void getTestsExecutionStatusFailure() {
		
		testExecutionEntity.getTestSuiteExecutionList().getFirst().getTestCaseExecutionList().getFirst().setStatus(TestStatusEnum.FAILURE);
		
		given(testExecutionRepository.findByCustomComponentIdAndCustomComponentSpecificationIdOrderByIdDesc(anyLong(), anyLong())).willReturn(Arrays.asList(testExecutionEntity));
		given(testExecutionMapper.createFromEntity(any(TestExecutionEntity.class))).willReturn(testExecution);
		given(testCaseMapper.createFromExecutionEntity(any(TestCaseExecutionEntity.class))).willReturn(testCase);

		TestSessionStatus testSessionStatus = testExecutionService.getTestsExecutionStatus(1L, 1L);
		
	    assertThat(testSessionStatus).isNotNull();		
			
	}
	
	@DisplayName("Test for TestExecutionService.getTestsExecutionStatus - Success")
    @Test
    void getTestsExecutionStatusSuccess() {
		
		testExecutionEntity.getTestSuiteExecutionList().getFirst().getTestCaseExecutionList().getFirst().setStatus(TestStatusEnum.SUCCESS);
		
		given(testExecutionRepository.findByCustomComponentIdAndCustomComponentSpecificationIdOrderByIdDesc(anyLong(), anyLong())).willReturn(Arrays.asList(testExecutionEntity));
		given(testExecutionMapper.createFromEntity(any(TestExecutionEntity.class))).willReturn(testExecution);
		given(testCaseMapper.createFromExecutionEntity(any(TestCaseExecutionEntity.class))).willReturn(testCase);

		TestSessionStatus testSessionStatus = testExecutionService.getTestsExecutionStatus(1L, 1L);
		
	    assertThat(testSessionStatus).isNotNull();		
			
	}
	
	@DisplayName("Test for TestExecutionService.getTestCaseExecutionLogs")
    @Test
    void getTestCaseExecutionLogs() {
		
		testCaseExecutionEntity.setLogs(Arrays.asList("test-log"));
				
		given(testCaseExecutionRepository.findBySession(anyString())).willReturn(testCaseExecutionEntity);

		TestCaseExecutionLogs testCaseExecutionLogs = testExecutionService.getTestCaseExecutionLogs("");
		
	    assertThat(testCaseExecutionLogs).isNotNull();		
			
	}
		
	@DisplayName("Test for TestExecutionService.getTestCaseExecutionReports")
    @Test
    void getTestCaseExecutionReports() {
				
		testCaseExecutionEntity.setReport("test-report");
		
		given(testCaseExecutionRepository.findBySession(anyString())).willReturn(testCaseExecutionEntity);

		TestCaseExecutionReport testCaseExecutionReport = testExecutionService.getTestCaseExecutionReport("");
		
	    assertThat(testCaseExecutionReport).isNotNull();		
			
	}
	
}