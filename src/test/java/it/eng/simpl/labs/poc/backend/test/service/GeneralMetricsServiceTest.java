package it.eng.simpl.labs.poc.backend.test.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.lenient;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import it.eng.simpl.labs.poc.entity.CurrentMetricsEntity;
import it.eng.simpl.labs.poc.enumeration.DataspaceStatusEnum;
import it.eng.simpl.labs.poc.model.CurrentConsumption;
import it.eng.simpl.labs.poc.model.Dataspace;
import it.eng.simpl.labs.poc.model.HistoricalConsumption;
import it.eng.simpl.labs.poc.model.TenantConsumption;
import it.eng.simpl.labs.poc.model.User;
import it.eng.simpl.labs.poc.repository.CurrentMetricsRepository;
import it.eng.simpl.labs.poc.repository.HistoricalMetricsRepository;
import it.eng.simpl.labs.poc.service.DataspaceService;
import it.eng.simpl.labs.poc.service.GeneralMetricsService;
import it.eng.simpl.labs.poc.service.interf.IK8sClusterManagementService;
import it.eng.simpl.labs.poc.utils.Constants;

@ExtendWith(MockitoExtension.class)
class GeneralMetricsServiceTest {

	@InjectMocks
	private GeneralMetricsService metricsService;
	
	@Mock
	private DataspaceService dataspaceService;
	
	@Mock
	private HistoricalMetricsRepository historicalMetricsRepository;
	
	@Mock
	private CurrentMetricsRepository currentMetricsRepository;
		
	@Mock
	private IK8sClusterManagementService k8sClusterManagementService; 

	private User user;
	
	private Dataspace dataspace;

	List<Object[]> objectsArray;
		
	private CurrentMetricsEntity currentMetricsEntity;
		
	@BeforeEach
	public void setup(){

		user=User.builder().email("test@test.com").username("testUser").build();

		dataspace = Dataspace.builder()
				.name("my-dataspace")
				.status(DataspaceStatusEnum.RUNNING)
				.build();
		
		objectsArray = new ArrayList<Object[]>();
        long millis=System.currentTimeMillis();  
        java.sql.Date date=new java.sql.Date(millis); 
		Object[] object = {date,"2","3","4","5","6","7"};
		objectsArray.add(object);
				
		currentMetricsEntity = new CurrentMetricsEntity();

		currentMetricsEntity.setCpuAllocated(Constants.ZERO);
		currentMetricsEntity.setCpuUsed(Constants.ZERO);
		currentMetricsEntity.setMemoryAllocated(Constants.ZERO);
		currentMetricsEntity.setMemoryUsed(Constants.ZERO);
		currentMetricsEntity.setStorageAllocated(Constants.ZERO);
		currentMetricsEntity.setStorageUsed(Constants.ZERO);
		currentMetricsEntity.setDataspaceId(1L);
		currentMetricsEntity.setNodeId(1L);

	}

	@DisplayName("Test for GeneralMetricsService.getHistoricalCpuMetrics")
	@Test
	void getHistoricalCpuMetrics() {
		
		given(historicalMetricsRepository.getGeneralHistoricalMetrics(any(LocalDate.class), any(LocalDate.class), anyString())).willReturn(objectsArray);		
		lenient().when(dataspaceService.findById(anyLong())).thenReturn(dataspace);

		List<HistoricalConsumption> historicalDataspacesConsumptionList = metricsService.getHistoricalCpuMetrics(LocalDate.now(), LocalDate.now(), user);

		assertThat(historicalDataspacesConsumptionList).isNotNull().hasSize(1);

	}
	
	@DisplayName("Test for GeneralMetricsService.getHistoricalMemoryMetrics")
	@Test
	void getHistoricalMemoryMetrics() {
		
		given(historicalMetricsRepository.getGeneralHistoricalMetrics(any(LocalDate.class), any(LocalDate.class), anyString())).willReturn(objectsArray);		
		lenient().when(dataspaceService.findById(anyLong())).thenReturn(dataspace);

		List<HistoricalConsumption> historicalDataspacesConsumptionList = metricsService.getHistoricalMemoryMetrics(LocalDate.now(), LocalDate.now(), user);

		assertThat(historicalDataspacesConsumptionList).isNotNull().hasSize(1);

	}
	
	@DisplayName("Test for GeneralMetricsService.getHistoricalStorageMetrics")
	@Test
	void getHistoricalStorageMetrics() {
		
		given(historicalMetricsRepository.getGeneralHistoricalMetrics(any(LocalDate.class), any(LocalDate.class), anyString())).willReturn(objectsArray);		
		lenient().when(dataspaceService.findById(anyLong())).thenReturn(dataspace);

		List<HistoricalConsumption> historicalDataspacesConsumptionList = metricsService.getHistoricalStorageMetrics(LocalDate.now(), LocalDate.now(), user);

		assertThat(historicalDataspacesConsumptionList).isNotNull().hasSize(1);

	}
		
	@DisplayName("Test for GeneralMetricsService.getCurrentCpuMetrics")
	@Test
	void getCurrentCpuMetrics() {

		given(currentMetricsRepository.getGeneralMetrics(user.getUsername())).willReturn(Arrays.asList(currentMetricsEntity));
		given(k8sClusterManagementService.getAllocatableResources(anyString())).willReturn(new BigDecimal("1"));

		TenantConsumption tenantConsumption = metricsService.getCurrentCpuMetrics(user);

		assertThat(tenantConsumption).isNotNull();
	}

	@DisplayName("Test for GeneralMetricsService.getCurrentMemoryMetrics")
	@Test
	void getCurrentMemoryMetrics() {

		given(currentMetricsRepository.getGeneralMetrics(user.getUsername())).willReturn(Arrays.asList(currentMetricsEntity));
		given(k8sClusterManagementService.getAllocatableResources(anyString())).willReturn(new BigDecimal("1"));

		TenantConsumption tenantConsumption = metricsService.getCurrentMemoryMetrics(user);

		assertThat(tenantConsumption).isNotNull();
	}
	
	@DisplayName("Test for GeneralMetricsService.getCurrentStorageMetrics")
	@Test
	void getCurrentStorageMetrics() {

		given(currentMetricsRepository.getGeneralMetrics(user.getUsername())).willReturn(Arrays.asList(currentMetricsEntity));
		
		TenantConsumption tenantConsumption = metricsService.getCurrentStorageMetrics(user);

		assertThat(tenantConsumption).isNotNull();
	}
	
	@DisplayName("Test for GeneralMetricsService.getCurrentMetrics")
	@Test
	void getCurrentMetrics() {

		given(currentMetricsRepository.getGeneralMetrics(user.getUsername())).willReturn(Arrays.asList(currentMetricsEntity));
		given(dataspaceService.findById(anyLong())).willReturn(dataspace);

		List<CurrentConsumption> currentDataspacesConsumptionList = metricsService.getCurrentMetrics(user);

		assertThat(currentDataspacesConsumptionList).isNotNull().hasSize(1);
	}	
}