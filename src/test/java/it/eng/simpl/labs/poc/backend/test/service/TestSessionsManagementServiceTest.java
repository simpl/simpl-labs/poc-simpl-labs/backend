package it.eng.simpl.labs.poc.backend.test.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyList;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.lenient;

import java.util.Arrays;
import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import it.eng.simpl.labs.poc.entity.TestCaseEntity;
import it.eng.simpl.labs.poc.entity.TestCaseEntityPK;
import it.eng.simpl.labs.poc.entity.TestCaseExecutionEntity;
import it.eng.simpl.labs.poc.entity.TestExecutionEntity;
import it.eng.simpl.labs.poc.entity.TestSuiteEntity;
import it.eng.simpl.labs.poc.entity.TestSuiteExecutionEntity;
import it.eng.simpl.labs.poc.exception.GenericException;
import it.eng.simpl.labs.poc.model.CustomComponent;
import it.eng.simpl.labs.poc.model.TestSessionStartRequest;
import it.eng.simpl.labs.poc.model.TestSessionStartResponse;
import it.eng.simpl.labs.poc.model.itb.SessionCreationInformation;
import it.eng.simpl.labs.poc.model.itb.SessionStopRequest;
import it.eng.simpl.labs.poc.model.itb.SessionStopResponse;
import it.eng.simpl.labs.poc.repository.TestCaseExecutionRepository;
import it.eng.simpl.labs.poc.repository.TestCaseRepository;
import it.eng.simpl.labs.poc.repository.TestExecutionRepository;
import it.eng.simpl.labs.poc.repository.TestSuiteExecutionRepository;
import it.eng.simpl.labs.poc.repository.TestSuiteRepository;
import it.eng.simpl.labs.poc.service.CustomComponentService;
import it.eng.simpl.labs.poc.service.ItbTestSessionsManagementService;
import it.eng.simpl.labs.poc.service.TestSessionsManagementService;

@ExtendWith(MockitoExtension.class)
class TestSessionsManagementServiceTest {

	@InjectMocks
	private TestSessionsManagementService testSessionsManagementService;
	
	@Mock
	private TestCaseRepository testCaseRepository;
	
	@Mock
	private TestSuiteRepository testSuiteRepository;
	
	@Mock
	private TestExecutionRepository testExecutionRepository;
	
	@Mock
	private TestSuiteExecutionRepository testSuiteExecutionRepository;
	
	@Mock
	private TestCaseExecutionRepository testCaseExecutionRepository;
	
	@Mock
	private CustomComponentService customComponentService;
	
	@Mock
	private ItbTestSessionsManagementService itbTestSessionsManagementService;
	
	private TestCaseEntity testCaseEntity;
	
	private TestSuiteEntity testSuiteEntity;
	
	private CustomComponent customComponent;
	
	private TestSessionStartResponse testSessionStartResponse;
	
	private TestExecutionEntity testExecutionEntity;
	
	private TestSuiteExecutionEntity testSuiteExecutionEntity;
	
	private TestCaseExecutionEntity testCaseExecutionEntity;
	
	private SessionStopResponse sessionStopResponse;
	
	private String testSuiteId;

	private String testCaseId;
	
	@BeforeEach
	public void setup(){
		
		testSuiteId = "TS1";
		testCaseId = "TC1";
		
		testCaseEntity = new TestCaseEntity();
		TestCaseEntityPK testCaseEntityPK = new TestCaseEntityPK();
		testCaseEntityPK.setTestCaseId(testCaseId);
		testCaseEntityPK.setTestSuiteId(testSuiteId);
		testCaseEntity.setId(testCaseEntityPK);
		testCaseEntity.setTestSuite(new TestSuiteEntity());
		testCaseEntity.getTestSuite().setId(testSuiteId);
		
		testSuiteEntity = new TestSuiteEntity();
		testSuiteEntity.setId(testSuiteId);
		
		customComponent = CustomComponent.builder()
				.system("my-system")
				.build();
		
		SessionCreationInformation sessionCreationInformation = new SessionCreationInformation();
		sessionCreationInformation.setSession("my-session");
		sessionCreationInformation.setTestCase(testCaseId);
		sessionCreationInformation.setTestSuite(testSuiteId);
		testSessionStartResponse = TestSessionStartResponse.builder()
				.createdSessions(Arrays.asList(sessionCreationInformation))
				.build();
	
		testExecutionEntity = new TestExecutionEntity();
		
		testSuiteExecutionEntity = new TestSuiteExecutionEntity();
		
		testCaseExecutionEntity = new TestCaseExecutionEntity();
		testCaseExecutionEntity.setTestSuiteExecution(new TestSuiteExecutionEntity());
		testCaseExecutionEntity.getTestSuiteExecution().setTestExecution(new TestExecutionEntity());
		testCaseExecutionEntity.getTestSuiteExecution().getTestExecution().setId(1L);
		
		sessionStopResponse = SessionStopResponse.builder().build();
	}
	
	@DisplayName("Test for TestSessionsManagementService.startTestSession")
    @Test
    void startTestSession() {
		
		TestSessionStartRequest testSessionStartRequest = TestSessionStartRequest.builder()
				.customComponentId(1L)
				.testSuite(Arrays.asList(testSuiteId))
				.testCase(Arrays.asList(testCaseId))
				.build();

		given(testCaseRepository.findByIdTestCaseIdIn(anyList())).willReturn(Arrays.asList(testCaseEntity));
		given(testSuiteRepository.findByIdIn(anyList())).willReturn(Arrays.asList(testSuiteEntity));
		given(customComponentService.findById(anyLong())).willReturn(customComponent);
		lenient().when(itbTestSessionsManagementService.startTestSession(any(TestSessionStartRequest.class))).thenReturn(testSessionStartResponse);
		given(testExecutionRepository.saveAndFlush(any(TestExecutionEntity.class))).willReturn(testExecutionEntity);
		given(testSuiteExecutionRepository.saveAndFlush(any(TestSuiteExecutionEntity.class))).willReturn(testSuiteExecutionEntity);
		lenient().when(testCaseExecutionRepository.saveAndFlush(any(TestCaseExecutionEntity.class))).thenReturn(testCaseExecutionEntity);
			
	    assertThat(testSessionsManagementService.startTestSession(testSessionStartRequest)).isNotNull();
			
	}
	
	@DisplayName("Test for TestSessionsManagementService.startTestSession - error")
    @Test
    void startTestSessionError() {
		
		TestSessionStartRequest testSessionStartRequest = TestSessionStartRequest.builder()
				.testSuite(Arrays.asList(testSuiteId))
				.build();

		given(testSuiteRepository.findByIdIn(anyList())).willReturn(null);
			
	    Exception exception = assertThrows(GenericException.class, () -> {
	    	testSessionsManagementService.startTestSession(testSessionStartRequest);
	    });
	    
	    String expectedMessage = "The request doesn't match the local data base configuration";
	    String actualMessage = exception.getMessage();

	    assertTrue(actualMessage.contains(expectedMessage));
	    
	}
	
	@DisplayName("Test for TestSessionsManagementService.stopTestSession")
    @Test
    void stopTestSession() {
		
		SessionStopRequest sessionStopRequest = new SessionStopRequest();
		sessionStopRequest.setSession(Arrays.asList("session-1"));

		given(itbTestSessionsManagementService.stopTestSession(any(SessionStopRequest.class))).willReturn(sessionStopResponse);
		lenient().when(testExecutionRepository.findById(anyLong())).thenReturn(Optional.of(testExecutionEntity));
		lenient().when(testCaseExecutionRepository.findBySession(anyString())).thenReturn(testCaseExecutionEntity);
					
	    assertThat(testSessionsManagementService.stopTestSession(sessionStopRequest)).isNotNull();
			
	}
}