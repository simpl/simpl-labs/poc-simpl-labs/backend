package it.eng.simpl.labs.poc.backend.test.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.lenient;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import it.eng.simpl.labs.poc.entity.CurrentMetricsEntity;
import it.eng.simpl.labs.poc.entity.HistoricalMetricsEntity;
import it.eng.simpl.labs.poc.entity.NodeEntity;
import it.eng.simpl.labs.poc.enumeration.NodeTypeEnum;
import it.eng.simpl.labs.poc.repository.CurrentMetricsRepository;
import it.eng.simpl.labs.poc.repository.HistoricalMetricsRepository;
import it.eng.simpl.labs.poc.repository.NodeRepository;
import it.eng.simpl.labs.poc.service.MetricsManagementService;
import it.eng.simpl.labs.poc.service.interf.IK8sClusterManagementService;
import it.eng.simpl.labs.poc.utils.Constants;

@ExtendWith(MockitoExtension.class)
class MetricsManagementServiceTest {
	
	@InjectMocks
	private MetricsManagementService metricsManagementService;

	@Mock
	private HistoricalMetricsRepository historicalMetricsRepository;

	@Mock
	private CurrentMetricsRepository currentMetricsRepository;

	@Mock
	private NodeRepository nodeRepository;

	@Mock
	private IK8sClusterManagementService k8sClusterManagementService; 

	private HistoricalMetricsEntity historicalMetricsEntity;

	private CurrentMetricsEntity currentMetricsEntity;

	@BeforeEach
	public void setup(){

		historicalMetricsEntity = new HistoricalMetricsEntity();

		currentMetricsEntity = new CurrentMetricsEntity();
		currentMetricsEntity.setCpuAllocated(Constants.ZERO);
		currentMetricsEntity.setCpuUsed(Constants.ZERO);
		currentMetricsEntity.setMemoryAllocated(Constants.ZERO);
		currentMetricsEntity.setMemoryUsed(Constants.ZERO);
		currentMetricsEntity.setStorageAllocated(Constants.ZERO);
		currentMetricsEntity.setStorageUsed(Constants.ZERO);
		currentMetricsEntity.setDataspaceId(1L);
		currentMetricsEntity.setNodeId(1L);

	}

	@DisplayName("Test for MetricsManagementService.deleteByCreateDateLessThan")
	@Test
	void deleteByCreateDateLessThan() {

		List<HistoricalMetricsEntity> historicalMetricsEntityList = new ArrayList<>();
		historicalMetricsEntityList.add(historicalMetricsEntity);

		doNothing().when(historicalMetricsRepository).deleteByCreateDateLessThan(any(LocalDateTime.class));

	    assertDoesNotThrow(() -> {
			metricsManagementService.deleteByCreateDateLessThan(LocalDateTime.now());
	    });	

	}

	@DisplayName("Test for MetricsManagementService.deleteOrphans")
	@Test
	void deleteOrphans() {

		doNothing().when(historicalMetricsRepository).deleteOrphans();

	    assertDoesNotThrow(() -> {
			metricsManagementService.deleteOrphans();
	    });	

	}

	@DisplayName("Test for MetricsManagementService.insertBufferedMetrics")
	@Test
	void insertBufferedMetrics() {
		
		NodeEntity localNodeEntity = new NodeEntity();
		localNodeEntity.setId(1L);
		localNodeEntity.setType(NodeTypeEnum.APPLICATION_PROVIDER);

		given(k8sClusterManagementService.getCurrentMetrics()).willReturn(Arrays.asList(currentMetricsEntity));
		lenient().when(nodeRepository.findById(anyLong())).thenReturn(Optional.of(localNodeEntity));
		given(currentMetricsRepository.saveAllAndFlush(any())).willReturn(Arrays.asList(currentMetricsEntity));		

		List<CurrentMetricsEntity> currentMetricsEntityList = metricsManagementService.updateBufferedMetrics();

		assertThat(currentMetricsEntityList).isNotNull().hasSize(1);
	}
	
	@DisplayName("Test for MetricsManagementService.deleteOldMetrics")
	@Test
	void deleteOldMetrics() {

		doNothing().when(historicalMetricsRepository).deleteByCreateDateLessThan(any(LocalDateTime.class));

		assertDoesNotThrow(() -> {
			metricsManagementService.deleteOldMetrics(LocalDateTime.now());
	    });	
	}
	
	@DisplayName("Test for MetricsManagementService.updateMetrics")
	@Test
	void updateMetrics() {
		
		List<HistoricalMetricsEntity> historicalMetricsEntityList = new ArrayList<>();
		
		given(historicalMetricsRepository.saveAllAndFlush(any())).willReturn(historicalMetricsEntityList);	

		assertDoesNotThrow(() -> {
			metricsManagementService.updateMetrics();
	    });	
	}
	
}