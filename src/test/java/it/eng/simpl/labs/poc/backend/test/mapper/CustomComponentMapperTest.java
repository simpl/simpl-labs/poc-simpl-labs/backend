package it.eng.simpl.labs.poc.backend.test.mapper;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.BDDMockito.given;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.PageRequest;

import it.eng.simpl.labs.poc.entity.CustomComponentEntity;
import it.eng.simpl.labs.poc.entity.SpecificationEntity;
import it.eng.simpl.labs.poc.entity.TestCaseEntity;
import it.eng.simpl.labs.poc.entity.TestCaseEntityPK;
import it.eng.simpl.labs.poc.entity.TestCaseExecutionEntity;
import it.eng.simpl.labs.poc.entity.TestExecutionEntity;
import it.eng.simpl.labs.poc.entity.TestSuiteEntity;
import it.eng.simpl.labs.poc.enumeration.CategoryTypeEnum;
import it.eng.simpl.labs.poc.enumeration.NodeTypeEnum;
import it.eng.simpl.labs.poc.enumeration.TestStatusEnum;
import it.eng.simpl.labs.poc.mapper.CustomComponentMapper;
import it.eng.simpl.labs.poc.model.CustomComponent;
import it.eng.simpl.labs.poc.repository.TestCaseExecutionRepository;
import it.eng.simpl.labs.poc.repository.TestCaseRepository;
import it.eng.simpl.labs.poc.repository.TestSuiteRepository;

@ExtendWith(MockitoExtension.class)
class CustomComponentMapperTest {
	
	@InjectMocks
	private CustomComponentMapper customComponentMapper;

	@Mock
	private TestCaseRepository testCaseRepository;
	
	@Mock
	private TestCaseExecutionRepository testCaseExecutionRepository;
	
	@Mock
	private TestSuiteRepository testSuiteRepository;
	
	private CustomComponent customComponent;
	
	private CustomComponentEntity customComponentEntity;

	private List<CustomComponentEntity> componentEntityList;

	private List<CustomComponent> customComponentList;
	
	private TestCaseEntity testCaseEntity;
	
	private List<TestCaseExecutionEntity> testCaseExecutionEntityList;
	
	private TestSuiteEntity testSuiteEntity;
	
	@BeforeEach
	public void setup(){
		
		customComponent = CustomComponent.builder()
				.id(1L)
				.build();
		
		customComponentList = new ArrayList<>();
		customComponentList.add(customComponent);

		customComponentEntity = new CustomComponentEntity();
		customComponentEntity.setId(1L);
		SpecificationEntity specificationEntity = new SpecificationEntity();
		specificationEntity.setId(1L);
		customComponentEntity.setSpecification(specificationEntity);
		customComponentEntity.setCategory(CategoryTypeEnum.APPLICATION_SHARING);
		customComponentEntity.setNodeType(NodeTypeEnum.APPLICATION_PROVIDER);
		customComponentEntity.setSpecificationId(1L);
		List<TestExecutionEntity> testExecutionEntityList = new ArrayList<>();
		TestExecutionEntity testExecutionEntity = new TestExecutionEntity();
		testExecutionEntity.setStatus(TestStatusEnum.UNDEFINED);
		testExecutionEntityList.add(testExecutionEntity);
		customComponentEntity.setTestexecutionList(testExecutionEntityList);
		componentEntityList = new ArrayList<>();
		componentEntityList.add(customComponentEntity);
		
		testCaseEntity = new TestCaseEntity();
		TestCaseEntityPK testCaseEntityPK = new TestCaseEntityPK();
		testCaseEntityPK.setTestCaseId("1");
		testCaseEntityPK.setTestSuiteId("1");
		testCaseEntity.setId(testCaseEntityPK);
		
		testCaseExecutionEntityList = new ArrayList<>();
		TestCaseExecutionEntity testCaseExecutionEntity = new TestCaseExecutionEntity();
		testCaseExecutionEntity.setStatus(TestStatusEnum.SUCCESS);
		testCaseExecutionEntityList.add(testCaseExecutionEntity);
		
		testSuiteEntity = new TestSuiteEntity();
		testSuiteEntity.setId("1");
	}
	
	@DisplayName("Test for CustomComponentMapper.createFromEntity - Success")
    @Test
    void createFromEntityOk(){
				
		given(testCaseRepository.countTotals(anyLong())).willReturn(1);
		given(testCaseRepository.findByTestSuiteId(anyString())).willReturn(Arrays.asList(testCaseEntity));
		given(testCaseExecutionRepository.findByTestCaseExecution(anyString(), anyString(), anyLong(), any(PageRequest.class))).willReturn(testCaseExecutionEntityList);
		given(testSuiteRepository.findBySpecificationId(anyLong())).willReturn(Arrays.asList(testSuiteEntity));

		CustomComponent localCustomComponent = customComponentMapper.createFromEntity(customComponentEntity);
		assertThat(localCustomComponent).isNotNull().hasFieldOrPropertyWithValue("id", customComponentEntity.getId());
		
		customComponentEntity.getTestexecutionList().get(0).setStatus(TestStatusEnum.SUCCESS);
		localCustomComponent = customComponentMapper.createFromEntity(customComponentEntity);
		assertThat(localCustomComponent).isNotNull().hasFieldOrPropertyWithValue("id", customComponentEntity.getId());
		
		testCaseExecutionEntityList.get(0).setStatus(TestStatusEnum.FAILURE);
		localCustomComponent = customComponentMapper.createFromEntity(customComponentEntity);
		assertThat(localCustomComponent).isNotNull().hasFieldOrPropertyWithValue("id", customComponentEntity.getId());
	}
	
	@DisplayName("Test for CustomComponentMapper.createFromDto - Success")
	@Test
	void createFromDtoOk(){

		Exception exception = assertThrows(UnsupportedOperationException.class, () -> {
			customComponentMapper.createFromDto(customComponent);
		});

		String expectedMessage = "Not yet implemented";
		String actualMessage = exception.getMessage();

		assertTrue(actualMessage.contains(expectedMessage));

	}
	
	
}
