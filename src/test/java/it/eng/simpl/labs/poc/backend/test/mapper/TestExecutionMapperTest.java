package it.eng.simpl.labs.poc.backend.test.mapper;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.time.LocalDateTime;
import java.util.Arrays;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;

import it.eng.simpl.labs.poc.entity.CustomComponentEntity;
import it.eng.simpl.labs.poc.entity.SpecificationEntity;
import it.eng.simpl.labs.poc.entity.TestCaseExecutionEntity;
import it.eng.simpl.labs.poc.entity.TestExecutionEntity;
import it.eng.simpl.labs.poc.entity.TestSuiteExecutionEntity;
import it.eng.simpl.labs.poc.enumeration.TestStatusEnum;
import it.eng.simpl.labs.poc.mapper.TestExecutionMapper;
import it.eng.simpl.labs.poc.model.TestExecution;

@ExtendWith(MockitoExtension.class)
class TestExecutionMapperTest {
	@InjectMocks
	private TestExecutionMapper testExecutionMapper;
	
	private TestExecutionEntity testExecutionEntity;
	private TestSuiteExecutionEntity testSuiteExecutionEntity;
	private CustomComponentEntity customComponentEntity;
	private TestCaseExecutionEntity testCaseExecutionEntity;
	
	@BeforeEach
	public void setup(){
		testCaseExecutionEntity=new TestCaseExecutionEntity();
		testCaseExecutionEntity.setStatus(TestStatusEnum.SUCCESS);
		
		testSuiteExecutionEntity=new TestSuiteExecutionEntity();
		testSuiteExecutionEntity.setStatus(TestStatusEnum.SUCCESS);
		testSuiteExecutionEntity.setStartDate(LocalDateTime.now().minusDays(1));
		testSuiteExecutionEntity.setEndDate(LocalDateTime.now());
		testSuiteExecutionEntity.setTestCaseExecutionList(Arrays.asList(testCaseExecutionEntity));;
		
		customComponentEntity=new CustomComponentEntity();
		customComponentEntity.setSpecification(new SpecificationEntity());
		
		testExecutionEntity=new TestExecutionEntity();
		testExecutionEntity.setId(1L);
		testExecutionEntity.setStatus(TestStatusEnum.SUCCESS);
		testExecutionEntity.setTestSuiteExecutionList(Arrays.asList(testSuiteExecutionEntity));
		testExecutionEntity.setCustomComponent(customComponentEntity);
	}
	
	@DisplayName("Test for TestExecutionMapper.createFromEntity")
    @Test
    void givenTestExecutionEntity_thenReturnTestExecutionDto(){
		TestExecution returnedDto=testExecutionMapper.createFromEntity(testExecutionEntity);
		assertThat(returnedDto).isNotNull().hasFieldOrPropertyWithValue("id", testExecutionEntity.getId());
		
		testExecutionEntity.setStatus(TestStatusEnum.UNDEFINED);
		testExecutionEntity.setEndDate(null);
		returnedDto = testExecutionMapper.createFromEntity(testExecutionEntity);
		assertThat(returnedDto).isNotNull().hasFieldOrPropertyWithValue("id", testExecutionEntity.getId());
	}
	
	@DisplayName("Test for TestExecutionMapper.createFromDto")
	@Test
	void givenTestExecutionDto_thneReturnExecutionEntity() {

		assertThrows(UnsupportedOperationException.class, () -> {
			testExecutionMapper.createFromDto(null);
		});

	}
	
}
