package it.eng.simpl.labs.poc.backend.test.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.BDDMockito.given;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import it.eng.simpl.labs.poc.entity.TestCaseEntity;
import it.eng.simpl.labs.poc.entity.TestCaseEntityPK;
import it.eng.simpl.labs.poc.entity.TestCaseExecutionEntity;
import it.eng.simpl.labs.poc.enumeration.TestStatusEnum;
import it.eng.simpl.labs.poc.mapper.TestCaseMapper;
import it.eng.simpl.labs.poc.model.TestCase;
import it.eng.simpl.labs.poc.repository.TestCaseExecutionRepository;
import it.eng.simpl.labs.poc.repository.TestCaseRepository;
import it.eng.simpl.labs.poc.service.TestCaseService;

@ExtendWith(MockitoExtension.class)
public class TestCaseServiceTest {

	private TestCaseEntity testcaseEntity;
	private TestCaseExecutionEntity testCaseExecutionEntity;
	private TestCase testcaseModel;

	@Mock
	private TestCaseRepository testcaseRepository;

	@Mock
	private TestCaseMapper testcaseMapper;

	@Mock
	private TestCaseExecutionRepository testCaseExecutionRepository;

	@InjectMocks
	private TestCaseService testcaseService;

	@BeforeEach
	public void setup(){
		testcaseEntity=new TestCaseEntity();
		testcaseEntity.setCreateDate(null);
		testcaseEntity.setDescription("Example testcase");
		testcaseEntity.setId(new TestCaseEntityPK());
		testcaseEntity.getId().setTestCaseId("1");
		testcaseEntity.setName("My Unit Test Testcase");
		testcaseEntity.setSteps("1");
		testcaseEntity.setTestSuite(null);
		testcaseEntity.setUserId("usr");
		testcaseEntity.setVersion("1.0");
		testcaseModel=TestCase.builder().id("1").build();

		testCaseExecutionEntity=new TestCaseExecutionEntity();
		testCaseExecutionEntity.setId(123L);
		testCaseExecutionEntity.setStatus(TestStatusEnum.SUCCESS);
		testCaseExecutionEntity.setStartDate(LocalDateTime.now().minusDays(1));
		testCaseExecutionEntity.setEndDate(LocalDateTime.now().minusDays(1));
	}

	@DisplayName("Test for TestcaseService.findByTestSuiteId")
	@Test
	void givenTestsuiteIdAndComponentId_whenFind_thenTestCaseList() {
		String testSuiteId = "1";
		Long componentId = 1L;
		given(testcaseRepository.findByTestSuiteId(testSuiteId)).willReturn(Arrays.asList(testcaseEntity));
		given(testcaseMapper.createFromEntity(any(TestCaseEntity.class))).willReturn(testcaseModel);
		given(testCaseExecutionRepository.findLastExecution(eq("1"),eq(1L))).willReturn(Arrays.asList(testCaseExecutionEntity));

		List<TestCase> testCaseList = testcaseService.findByTestSuiteId(testSuiteId, componentId);

		assertThat(testCaseList).isNotNull()
		.hasSizeGreaterThan(0);		
	}

}