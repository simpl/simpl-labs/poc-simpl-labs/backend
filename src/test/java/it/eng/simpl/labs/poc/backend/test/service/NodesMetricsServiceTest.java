package it.eng.simpl.labs.poc.backend.test.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.BDDMockito.given;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;

import it.eng.simpl.labs.poc.entity.CategoryEntity;
import it.eng.simpl.labs.poc.entity.ComponentEntity;
import it.eng.simpl.labs.poc.entity.CurrentMetricsEntity;
import it.eng.simpl.labs.poc.entity.DataspaceEntity;
import it.eng.simpl.labs.poc.entity.NodeEntity;
import it.eng.simpl.labs.poc.enumeration.CategoryTypeEnum;
import it.eng.simpl.labs.poc.enumeration.ComponentTypeEnum;
import it.eng.simpl.labs.poc.enumeration.DataspaceStatusEnum;
import it.eng.simpl.labs.poc.enumeration.NodeTypeEnum;
import it.eng.simpl.labs.poc.model.CurrentConsumption;
import it.eng.simpl.labs.poc.model.HistoricalConsumption;
import it.eng.simpl.labs.poc.repository.CurrentMetricsRepository;
import it.eng.simpl.labs.poc.repository.HistoricalMetricsRepository;
import it.eng.simpl.labs.poc.service.NodesMetricsService;
import it.eng.simpl.labs.poc.utils.Constants;

@ExtendWith(MockitoExtension.class)
class NodesMetricsServiceTest {

	@InjectMocks
	private NodesMetricsService nodesMetricsService;
	
	@Mock
	private CurrentMetricsRepository currentMetricsRepository;
	
	@Mock
	private HistoricalMetricsRepository historicalMetricsRepository;

	private CurrentMetricsEntity currentMetricsEntity;
			
	List<Object[]> objectsArray;

	@BeforeEach
	public void setup(){

		currentMetricsEntity = new CurrentMetricsEntity();
		currentMetricsEntity.setCpuAllocated(Constants.ZERO);
		currentMetricsEntity.setCpuUsed(Constants.ZERO);
		currentMetricsEntity.setMemoryAllocated(Constants.ZERO);
		currentMetricsEntity.setMemoryUsed(Constants.ZERO);
		currentMetricsEntity.setStorageAllocated(Constants.ZERO);
		currentMetricsEntity.setStorageUsed(Constants.ZERO);
		currentMetricsEntity.setDataspaceId(1L);
		currentMetricsEntity.setNodeId(1L);
		
		objectsArray = new ArrayList<Object[]>();
        long millis=System.currentTimeMillis();  
        java.sql.Date date=new java.sql.Date(millis); 
		Object[] object = {date,"2","3","4","5","6","7"};
		objectsArray.add(object);
	}

	@DisplayName("Test for NodesMetricsService.getCurrentMetrics")
	@Test
	void getCurrentMetrics() {
		
        List<CurrentMetricsEntity> localCurrentMetricsEntityList = new ArrayList<>();
        CurrentMetricsEntity localCurrentMetricsEntity = new CurrentMetricsEntity();
        ComponentEntity localComponentEntity = new ComponentEntity();
        localComponentEntity.setId(1L);
        localComponentEntity.setType(ComponentTypeEnum.BE_COMPONENT);
        CategoryEntity localCategoryEntity = new CategoryEntity();
        localCategoryEntity.setId(1L);
        localCategoryEntity.setType(CategoryTypeEnum.APPLICATION_SHARING);
        NodeEntity localNodeEntity = new NodeEntity();
        localNodeEntity.setId(1L);
        localNodeEntity.setName("my-node");
        localNodeEntity.setType(NodeTypeEnum.APPLICATION_PROVIDER);
        DataspaceEntity localDataspaceEntity = new DataspaceEntity();
        localDataspaceEntity.setId(1L);
        localDataspaceEntity.setStatus(DataspaceStatusEnum.RUNNING);
        localDataspaceEntity.setName("my-dataspace");
        localNodeEntity.setDataspace(localDataspaceEntity);
        localCategoryEntity.setNode(localNodeEntity);
        localComponentEntity.setCategory(localCategoryEntity);
        localCurrentMetricsEntity.setComponent(localComponentEntity);
        localCurrentMetricsEntity.setMemoryUsed(BigDecimal.ZERO);
        localCurrentMetricsEntityList.add(localCurrentMetricsEntity);
		
		given(currentMetricsRepository.findByNodeId(anyLong())).willReturn(Arrays.asList(currentMetricsEntity));        
		given(currentMetricsRepository.findByNodeId(anyLong(), any(PageRequest.class))).willReturn(new PageImpl<>(localCurrentMetricsEntityList, PageRequest.of(1, 10), 1));        
		
		Page<CurrentConsumption> currentConsumptionPage = nodesMetricsService.getCurrentMetrics(1L, PageRequest.of(1, 10));
		
		assertNotNull(currentConsumptionPage);
	}
	
	@DisplayName("Test for NodesMetricsService.getHistoricalCpuMetrics")
	@Test
	void getHistoricalCpuMetrics() {

		given(historicalMetricsRepository.getNodesHistoricalMetrics(any(LocalDate.class), any(LocalDate.class), anyLong())).willReturn(objectsArray);		

		List<HistoricalConsumption> currentDataspacesConsumptionList = nodesMetricsService.getHistoricalCpuMetrics(1L, LocalDate.now(), LocalDate.now());

		assertThat(currentDataspacesConsumptionList).isNotNull().hasSize(1);
	}
	
	@DisplayName("Test for NodesMetricsService.getHistoricalMemoryMetrics")
	@Test
	void getHistoricalMemoryMetrics() {

		given(historicalMetricsRepository.getNodesHistoricalMetrics(any(LocalDate.class), any(LocalDate.class), anyLong())).willReturn(objectsArray);		

		List<HistoricalConsumption> currentDataspacesConsumptionList = nodesMetricsService.getHistoricalMemoryMetrics(1L, LocalDate.now(), LocalDate.now());

		assertThat(currentDataspacesConsumptionList).isNotNull().hasSize(1);
	}
	
	@DisplayName("Test for NodesMetricsService.getHistoricalStorageMetrics")
	@Test
	void getHistoricalStorageMetrics() {

		given(historicalMetricsRepository.getNodesHistoricalMetrics(any(LocalDate.class), any(LocalDate.class), anyLong())).willReturn(objectsArray);		

		List<HistoricalConsumption> currentDataspacesConsumptionList = nodesMetricsService.getHistoricalStorageMetrics(1L, LocalDate.now(), LocalDate.now());

		assertThat(currentDataspacesConsumptionList).isNotNull().hasSize(1);
	}

}