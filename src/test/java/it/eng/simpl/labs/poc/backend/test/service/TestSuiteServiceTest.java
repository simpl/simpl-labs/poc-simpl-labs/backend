package it.eng.simpl.labs.poc.backend.test.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.BDDMockito.given;

import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.Pageable;

import it.eng.simpl.labs.poc.entity.TestCaseEntity;
import it.eng.simpl.labs.poc.entity.TestCaseEntityPK;
import it.eng.simpl.labs.poc.entity.TestCaseExecutionEntity;
import it.eng.simpl.labs.poc.entity.TestSuiteEntity;
import it.eng.simpl.labs.poc.enumeration.TestStatusEnum;
import it.eng.simpl.labs.poc.mapper.TestSuiteMapper;
import it.eng.simpl.labs.poc.model.TestSuite;
import it.eng.simpl.labs.poc.repository.TestCaseExecutionRepository;
import it.eng.simpl.labs.poc.repository.TestCaseRepository;
import it.eng.simpl.labs.poc.repository.TestSuiteRepository;
import it.eng.simpl.labs.poc.service.TestSuiteService;

@ExtendWith(MockitoExtension.class)
class TestSuiteServiceTest {
	
	@InjectMocks
	private TestSuiteService testSuiteService;
	
	@Mock
	private TestCaseRepository testCaseRepository;
	
	@Mock
	private TestCaseExecutionRepository testCaseExecutionRepository;
	
	@Mock
	private TestSuiteRepository testSuiteRepository;
	
	@Mock
	private TestSuiteMapper testSuiteMapper;
	
	private TestCaseEntity testCaseEntity;
	
	private TestCaseExecutionEntity testCaseExecutionEntity;
	
	private TestSuiteEntity testSuiteEntity;
	
	private TestSuite testSuite;
	
	@BeforeEach
	public void setup(){
		
		testCaseEntity = new TestCaseEntity();
		TestCaseEntityPK testCaseEntityPK = new TestCaseEntityPK();
		testCaseEntityPK.setTestCaseId("1");
		testCaseEntityPK.setTestSuiteId("1");
		testCaseEntity.setId(testCaseEntityPK);
		
		testCaseExecutionEntity = new TestCaseExecutionEntity();
		
		testSuiteEntity = new TestSuiteEntity();
		testSuiteEntity.setId("1");
		
		testSuite = TestSuite.builder().build();

	}
	
	@DisplayName("Test for TestSuiteService.findBySpecificationId")
    @Test
    void findBySpecificationId() {
				
		given(testCaseRepository.findByTestSuiteId(anyString())).willReturn(Arrays.asList(testCaseEntity));
		given(testSuiteRepository.findBySpecificationId(anyLong())).willReturn(Arrays.asList(testSuiteEntity));
		given(testSuiteMapper.createFromEntity(any(TestSuiteEntity.class))).willReturn(testSuite);
		
		testCaseExecutionEntity.setStatus(TestStatusEnum.SUCCESS);
		given(testCaseExecutionRepository.findByTestCaseExecution(anyString(), anyString(), anyLong(), any(Pageable.class))).willReturn(Arrays.asList(testCaseExecutionEntity));
	    assertThat(testSuiteService.findBySpecificationId(1L, 1L)).isNotNull().hasSizeGreaterThan(0);	
	    
		testCaseExecutionEntity.setStatus(TestStatusEnum.FAILURE);
		given(testCaseExecutionRepository.findByTestCaseExecution(anyString(), anyString(), anyLong(), any(Pageable.class))).willReturn(Arrays.asList(testCaseExecutionEntity));
	    assertThat(testSuiteService.findBySpecificationId(1L, 1L)).isNotNull().hasSizeGreaterThan(0);
	}
	

}