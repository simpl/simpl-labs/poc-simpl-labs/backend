package it.eng.simpl.labs.poc.backend.test.mapper;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import it.eng.simpl.labs.poc.entity.DataspaceEntity;
import it.eng.simpl.labs.poc.entity.NodeEntity;
import it.eng.simpl.labs.poc.mapper.DataspaceMapper;
import it.eng.simpl.labs.poc.mapper.NodeMapper;
import it.eng.simpl.labs.poc.model.Dataspace;
import it.eng.simpl.labs.poc.model.Node;
import it.eng.simpl.labs.poc.model.User;

@ExtendWith(MockitoExtension.class)
class DataspaceMapperTest {
	
	@Mock
	private NodeMapper nodeMapper;
	
	@InjectMocks
	private DataspaceMapper dataspaceMapper;
	private DataspaceEntity dataspaceEntity;
	private Dataspace dataspace;
	private Stream<NodeEntity> nodeEntityStream;
	private Stream<Node> nodeStream;
	private List<NodeEntity> nodeEntityList;
	private List<Node> nodeList;
	
	@BeforeEach
	public void setup(){
		Node node=Node.builder().id(1L).build();
		nodeList=Arrays.asList(node);
		nodeStream=nodeList.stream();
		
		NodeEntity nodeEntity=new NodeEntity();
		nodeEntity.setId(1L);	
		nodeEntityList=Arrays.asList(nodeEntity);
		nodeEntityStream=nodeEntityList.stream();
		
		dataspace=Dataspace.builder().id(1L).build();
		dataspace.setNodeList(nodeList);
		dataspace.setUser(User.builder().username("test").build());
		
		dataspaceEntity=new DataspaceEntity();
		dataspaceEntity.setId(1L);	
		dataspaceEntity.setNodeList(nodeEntityList);
		dataspaceEntity.setUserId("test");
	}
	
	@DisplayName("Test for DataspaceMapper.createFromDto")
    @Test
    void givenDataspaceDto_thenReturnDataspaceEntity(){
		given(nodeMapper.createFromDtos(any(List.class)))
		.willReturn(nodeEntityStream);
		
		DataspaceEntity returnedEntity=dataspaceMapper.createFromDto(dataspace);
		assertThat(returnedEntity).isNotNull().hasFieldOrPropertyWithValue("id", dataspace.getId());
	}
	
	@DisplayName("Test for DataspaceMapper.createFromEntity")
    @Test
    void givenDataspaceEntity_thenReturnDataspaceDto(){
		given(nodeMapper.createFromEntities(any(List.class)))
		.willReturn(nodeStream);
		
		Dataspace returnedDto=dataspaceMapper.createFromEntity(dataspaceEntity);
		assertThat(returnedDto).isNotNull().hasFieldOrPropertyWithValue("id", dataspaceEntity.getId());
	}
	
}
