package it.eng.simpl.labs.poc.backend.test.mapper;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;

import it.eng.simpl.labs.poc.entity.ComponentEntity;
import it.eng.simpl.labs.poc.mapper.ComponentMapper;
import it.eng.simpl.labs.poc.model.Component;

@ExtendWith(MockitoExtension.class)
class ComponentMapperTest {
	@InjectMocks
	private ComponentMapper componentMapper;
	private ComponentEntity componentEntity;
	private Component component;
	
	@BeforeEach
	public void setup(){
		component=Component.builder().id(1L).build();
		componentEntity=new ComponentEntity();
		componentEntity.setId(1L);	
	}
	
	@DisplayName("Test for ComponentMapper.createFromDto")
    @Test
    void givenComponentDto_thenReturnComponentEntity(){
		ComponentEntity returnedEntity=componentMapper.createFromDto(component);
		assertThat(returnedEntity).isNotNull().hasFieldOrPropertyWithValue("id", component.getId());
	}
	
	@DisplayName("Test for ComponentMapper.createFromEntity")
    @Test
    void givenComponentEntity_thenReturnComponentDto(){
		Component returnedDto=componentMapper.createFromEntity(componentEntity);
		assertThat(returnedDto).isNotNull().hasFieldOrPropertyWithValue("id", componentEntity.getId());
	}
	
}
