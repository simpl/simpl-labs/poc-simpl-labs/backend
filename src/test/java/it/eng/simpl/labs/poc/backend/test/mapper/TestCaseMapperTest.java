package it.eng.simpl.labs.poc.backend.test.mapper;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.time.LocalDateTime;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import it.eng.simpl.labs.poc.entity.TestCaseEntity;
import it.eng.simpl.labs.poc.entity.TestCaseEntityPK;
import it.eng.simpl.labs.poc.entity.TestCaseExecutionEntity;
import it.eng.simpl.labs.poc.entity.TestSuiteEntity;
import it.eng.simpl.labs.poc.entity.TestSuiteExecutionEntity;
import it.eng.simpl.labs.poc.enumeration.TestStatusEnum;
import it.eng.simpl.labs.poc.mapper.TestCaseMapper;
import it.eng.simpl.labs.poc.model.TestCase;

@ExtendWith(MockitoExtension.class)
class TestCaseMapperTest {

	@Mock
	private ObjectMapper mapper;

	@InjectMocks
	private TestCaseMapper testCaseMapper;

	private TestCaseEntity testCaseEntity;
	private TestCaseExecutionEntity testCaseExecutionEntity;

	@BeforeEach
	public void setup() {
		testCaseEntity = new TestCaseEntity();
		TestCaseEntityPK testCaseEntityPK = new TestCaseEntityPK();
		testCaseEntityPK.setTestCaseId("123");
		testCaseEntity.setId(testCaseEntityPK);
		testCaseEntity.setName("123CASE");
		testCaseEntity.setSteps("123STEP");
		testCaseEntity.setDescription("desc");
		TestSuiteExecutionEntity testSuiteExecutionEntity=new TestSuiteExecutionEntity();
		TestSuiteEntity testSuiteEntity=new TestSuiteEntity();
		testSuiteExecutionEntity.setTestSuite(testSuiteEntity);
		
		testCaseExecutionEntity = new TestCaseExecutionEntity();
		testCaseExecutionEntity.setTestCase(testCaseEntity);
		testCaseExecutionEntity.setStatus(TestStatusEnum.SUCCESS);
		testCaseExecutionEntity.setEndDate(LocalDateTime.now());
		testCaseExecutionEntity.setTestSuiteExecution(testSuiteExecutionEntity);
	}

	@DisplayName("Test for TestCaseMapper.createFromDto")
	@Test
	void testCreateFromDto() {
		Throwable exception = assertThrows(UnsupportedOperationException.class,
				() -> testCaseMapper.createFromDto(null));
		assertEquals("Not yet implemented", exception.getMessage());
	}

	@DisplayName("Test for TestCaseMapper.createFromEntity")
	@Test
	void testCreateFromEntity() {
		TestCase testCase = testCaseMapper.createFromEntity(testCaseEntity);
		assertThat(testCase).isNotNull().hasFieldOrPropertyWithValue("id", testCaseEntity.getId().getTestCaseId());
	}

	@DisplayName("Test for TestCaseMapper.createFromExecutionEntity")
	@Test
	void testCreateFromExecutionEntity() {
		
		TestCase testCase = testCaseMapper.createFromExecutionEntity(testCaseExecutionEntity);
		assertThat(testCase).isNotNull().hasFieldOrPropertyWithValue("id",
				testCaseExecutionEntity.getTestCase().getId().getTestCaseId());
		
		testCaseExecutionEntity.setStatus(TestStatusEnum.UNDEFINED);
		testCaseExecutionEntity.setEndDate(null);
		testCase = testCaseMapper.createFromExecutionEntity(testCaseExecutionEntity);
		assertThat(testCase).isNotNull().hasFieldOrPropertyWithValue("id",
				testCaseExecutionEntity.getTestCase().getId().getTestCaseId());
	}

}
