package it.eng.simpl.labs.poc.backend.test.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.test.util.ReflectionTestUtils;

import it.eng.simpl.labs.poc.exception.GenericException;
import it.eng.simpl.labs.poc.model.TestSessionStartRequest;
import it.eng.simpl.labs.poc.model.TestSessionStartResponse;
import it.eng.simpl.labs.poc.model.itb.SessionCreationInformation;
import it.eng.simpl.labs.poc.model.itb.SessionStartRequest;
import it.eng.simpl.labs.poc.model.itb.SessionStartResponse;
import it.eng.simpl.labs.poc.model.itb.SessionStatusRequest;
import it.eng.simpl.labs.poc.model.itb.SessionStatusResponse;
import it.eng.simpl.labs.poc.model.itb.SessionStopRequest;
import it.eng.simpl.labs.poc.model.itb.SessionStopResponse;
import it.eng.simpl.labs.poc.service.ItbClientService;
import it.eng.simpl.labs.poc.service.ItbTestSessionsManagementService;

@ExtendWith(MockitoExtension.class)
class ItbTestSessionsManagementServiceTest {

	@InjectMocks
	private ItbTestSessionsManagementService itbTestSessionsManagementService;

	@Mock
	private ItbClientService itbClientService;
	
	private String system1Name = "system-1";

	@BeforeEach
	public void setup(){

		ReflectionTestUtils.setField(itbTestSessionsManagementService, "sistem1name", system1Name);

	}

	@DisplayName("Test for ItbTestSessionsManagementService.startTestSession")
	@Test
	void startTestSession() {

		SessionStartResponse sessionStartResponse = new SessionStartResponse();
		SessionCreationInformation sessionCreationInformation = new SessionCreationInformation();
		sessionStartResponse.setCreatedSessions(Arrays.asList(sessionCreationInformation));

		given(itbClientService.startTestSession(any(SessionStartRequest.class), anyString())).willReturn(sessionStartResponse);		

		TestSessionStartRequest testSessionStartRequest = TestSessionStartRequest.builder().build();

		TestSessionStartResponse testSessionStartResponse = itbTestSessionsManagementService.startTestSession(testSessionStartRequest);
		assertThat(testSessionStartResponse).isNotNull();
		
		testSessionStartRequest.setSystem(system1Name);
		testSessionStartResponse = itbTestSessionsManagementService.startTestSession(testSessionStartRequest);
		assertThat(testSessionStartResponse).isNotNull();

	}

	@DisplayName("Test for ItbTestSessionsManagementService.startTestSession - KO")
	@Test
	void startTestSessionKO() {

		when(itbClientService.startTestSession(any(SessionStartRequest.class), anyString())).thenThrow(new GenericException());		

		TestSessionStartRequest testSessionStartRequest = TestSessionStartRequest.builder().build();

		assertThrows(GenericException.class, () -> {
			itbTestSessionsManagementService.startTestSession(testSessionStartRequest);
		});
		
		assertThrows(GenericException.class, () -> {
			itbTestSessionsManagementService.startTestSession(null);
		});

		given(itbClientService.startTestSession(any(SessionStartRequest.class), anyString())).willReturn(null);
		
		assertThrows(GenericException.class, () -> {
			itbTestSessionsManagementService.startTestSession(testSessionStartRequest);
		});
	}

	@DisplayName("Test for ItbTestSessionsManagementService.stopTestSession")
	@Test
	void stopTestSession() {

		SessionStopResponse sessionStopResponse = SessionStopResponse.builder().build();

		given(itbClientService.stopTestSession(any(SessionStopRequest.class), anyString())).willReturn(sessionStopResponse);		

		SessionStopRequest sessionStopRequest = new SessionStopRequest();

		assertThat(itbTestSessionsManagementService.stopTestSession(sessionStopRequest)).isNotNull();

	}
	
	@DisplayName("Test for ItbTestSessionsManagementService.stopTestSession - KO")
	@Test
	void stopTestSessionKO() {

		when(itbClientService.stopTestSession(any(SessionStopRequest.class), anyString())).thenThrow(new GenericException());		

		SessionStopRequest sessionStopRequest = new SessionStopRequest();

		assertThrows(GenericException.class, () -> {
			itbTestSessionsManagementService.stopTestSession(sessionStopRequest);
		});
		
		assertThrows(GenericException.class, () -> {
			itbTestSessionsManagementService.stopTestSession(null);
		});

	}

	@DisplayName("Test for ItbTestSessionsManagementService.getTestSessionStatus")
	@Test
	void getTestSessionStatus() {

		SessionStatusResponse sessionStatusResponse = new SessionStatusResponse();

		List<String> testSessionsList = Arrays.asList("session-1");

		given(itbClientService.getSessionStatus(any(SessionStatusRequest.class), anyString())).willReturn(sessionStatusResponse);		

		assertThat(itbTestSessionsManagementService.getSessionStatus(testSessionsList, Boolean.FALSE, Boolean.FALSE)).isNotNull();

	}
	
	@DisplayName("Test for ItbTestSessionsManagementService.getTestSessionStatus - KO")
	@Test
	void getTestSessionStatusKO() {

		when(itbClientService.getSessionStatus(any(SessionStatusRequest.class), anyString())).thenThrow(new GenericException());		

		assertThrows(GenericException.class, () -> {
			itbTestSessionsManagementService.getSessionStatus(Arrays.asList("my-session"),  Boolean.FALSE, Boolean.FALSE);
		});
		
		assertThrows(GenericException.class, () -> {
			itbTestSessionsManagementService.getSessionStatus(null, Boolean.FALSE, Boolean.FALSE);
		});

	}

}