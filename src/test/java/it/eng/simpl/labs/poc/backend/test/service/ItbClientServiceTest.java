package it.eng.simpl.labs.poc.backend.test.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.RequestEntity;
import org.springframework.http.RequestEntity.BodyBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import it.eng.simpl.labs.poc.model.itb.ISessionRequest;
import it.eng.simpl.labs.poc.model.itb.SessionStartRequest;
import it.eng.simpl.labs.poc.model.itb.SessionStartResponse;
import it.eng.simpl.labs.poc.model.itb.SessionStatusRequest;
import it.eng.simpl.labs.poc.model.itb.SessionStatusResponse;
import it.eng.simpl.labs.poc.model.itb.SessionStopRequest;
import it.eng.simpl.labs.poc.model.itb.SessionStopResponse;
import it.eng.simpl.labs.poc.service.ItbClientService;
import it.eng.simpl.labs.poc.utils.Constants;

@ExtendWith(MockitoExtension.class)
class ItbClientServiceTest {

	@InjectMocks
	private ItbClientService itbClientService;

	@Mock
	private RestTemplate restTemplate;

	private String organisationApiKey = "my-organisation-api-key";

	private String basePath = "http://my-site.com";

	private String finalUri = "/my-action"; 

	@BeforeEach
	public void setup(){

		ReflectionTestUtils.setField(itbClientService, "basePath", basePath);
		ReflectionTestUtils.setField(itbClientService, "organisationApiKey", organisationApiKey);

	}

	@DisplayName("Test for ItbTestSessionsManagementService.startTestSession")
	@Test
	void startTestSession() {

		SessionStartRequest sessionStartRequest = new SessionStartRequest();

		SessionStartResponse sessionStartResponse = new SessionStartResponse();

		BodyBuilder requestBuilder = RequestEntity.method(
				HttpMethod.POST, 
				UriComponentsBuilder.fromHttpUrl(basePath).toUriString() + finalUri);
		requestBuilder.accept(MediaType.APPLICATION_JSON);
		requestBuilder.contentType(MediaType.APPLICATION_JSON);

		requestBuilder.header(Constants.ITB_API_KEY, organisationApiKey);
		requestBuilder.header(Constants.USER_AGENT, Constants.JAVA_SDK);

		RequestEntity<ISessionRequest> requestEntity = requestBuilder.body(sessionStartRequest);		

		ResponseEntity<SessionStartResponse> mockResponse = new ResponseEntity<SessionStartResponse>(sessionStartResponse, HttpStatus.OK);
		ParameterizedTypeReference<SessionStartResponse> localReturnType = new ParameterizedTypeReference<SessionStartResponse>() {};

		when(restTemplate.exchange(requestEntity, localReturnType)).thenReturn(mockResponse);

		assertThat((SessionStartResponse) itbClientService.startTestSession(sessionStartRequest, finalUri)).isNotNull();

	}
	
	@DisplayName("Test for ItbTestSessionsManagementService.stopTestSession")
	@Test
	void stopTestSession() {

		SessionStopRequest sessionStopRequest = new SessionStopRequest();

		SessionStopResponse dessionStopResponse = SessionStopResponse.builder().build();

		BodyBuilder requestBuilder = RequestEntity.method(
				HttpMethod.POST, 
				UriComponentsBuilder.fromHttpUrl(basePath).toUriString() + finalUri);
		requestBuilder.accept(MediaType.APPLICATION_JSON);
		requestBuilder.contentType(MediaType.APPLICATION_JSON);

		requestBuilder.header(Constants.ITB_API_KEY, organisationApiKey);
		requestBuilder.header(Constants.USER_AGENT, Constants.JAVA_SDK);

		RequestEntity<ISessionRequest> requestEntity = requestBuilder.body(sessionStopRequest);		

		ResponseEntity<SessionStopResponse> mockResponse = new ResponseEntity<SessionStopResponse>(dessionStopResponse, HttpStatus.OK);
		ParameterizedTypeReference<SessionStopResponse> localReturnType = new ParameterizedTypeReference<SessionStopResponse>() {};

		when(restTemplate.exchange(requestEntity, localReturnType)).thenReturn(mockResponse);

		assertThat((SessionStopResponse) itbClientService.stopTestSession(sessionStopRequest, finalUri)).isNotNull();

	}
	
	@DisplayName("Test for ItbTestSessionsManagementService.getSessionStatus")
	@Test
	void getSessionStatus() {

		SessionStatusRequest sessionStatusRequest = new SessionStatusRequest();

		SessionStatusResponse sessionStatusResponse = new SessionStatusResponse();

		BodyBuilder requestBuilder = RequestEntity.method(
				HttpMethod.POST, 
				UriComponentsBuilder.fromHttpUrl(basePath).toUriString() + finalUri);
		requestBuilder.accept(MediaType.APPLICATION_JSON);
		requestBuilder.contentType(MediaType.APPLICATION_JSON);

		requestBuilder.header(Constants.ITB_API_KEY, organisationApiKey);
		requestBuilder.header(Constants.USER_AGENT, Constants.JAVA_SDK);

		RequestEntity<ISessionRequest> requestEntity = requestBuilder.body(sessionStatusRequest);		

		ResponseEntity<SessionStatusResponse> mockResponse = new ResponseEntity<SessionStatusResponse>(sessionStatusResponse, HttpStatus.OK);
		ParameterizedTypeReference<SessionStatusResponse> localReturnType = new ParameterizedTypeReference<SessionStatusResponse>() {};

		when(restTemplate.exchange(requestEntity, localReturnType)).thenReturn(mockResponse);

		assertThat((SessionStatusResponse) itbClientService.getSessionStatus(sessionStatusRequest, finalUri)).isNotNull();

	}

}