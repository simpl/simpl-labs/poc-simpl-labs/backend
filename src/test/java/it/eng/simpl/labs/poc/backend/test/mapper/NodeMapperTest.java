package it.eng.simpl.labs.poc.backend.test.mapper;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import it.eng.simpl.labs.poc.entity.CategoryEntity;
import it.eng.simpl.labs.poc.entity.NodeEntity;
import it.eng.simpl.labs.poc.mapper.CategoryMapper;
import it.eng.simpl.labs.poc.mapper.NodeMapper;
import it.eng.simpl.labs.poc.model.Category;
import it.eng.simpl.labs.poc.model.Node;

@ExtendWith(MockitoExtension.class)
class NodeMapperTest {
	
	@Mock
	private CategoryMapper categoryMapper;
	
	@InjectMocks
	private NodeMapper nodeMapper;	
	
	private NodeEntity nodeEntity;
	private Node node;
	private Stream<CategoryEntity> categoryEntityStream;
	private Stream<Category> categoryStream;
	private List<CategoryEntity> categoryEntityList;
	private List<Category> categoryList;
	
	@BeforeEach
	public void setup(){
		Category category=Category.builder().id(1L).build();
		categoryList=Arrays.asList(category);
		categoryStream=categoryList.stream();
		
		CategoryEntity categoryEntity=new CategoryEntity();
		categoryEntity.setId(1L);	
		categoryEntityList=Arrays.asList(categoryEntity);
		categoryEntityStream=categoryEntityList.stream();
		
		node=Node.builder().id(1L).build();
		node.setCategoryList(categoryList);
		
		nodeEntity=new NodeEntity();
		nodeEntity.setId(1L);	
		nodeEntity.setCategoryList(categoryEntityList);
	}
	
	@DisplayName("Test for NodeMapper.createFromDto")
    @Test
    void givenNodeDto_thenReturnNodeEntity(){
		given(categoryMapper.createFromDtos(any(List.class)))
		.willReturn(categoryEntityStream);
		
		NodeEntity returnedEntity=nodeMapper.createFromDto(node);
		assertThat(returnedEntity).isNotNull().hasFieldOrPropertyWithValue("id", node.getId());
	}
	
	@DisplayName("Test for NodeMapper.createFromEntity")
    @Test
    void givenNodeEntity_thenReturnNodeDto(){
		given(categoryMapper.createFromEntities(any(List.class)))
		.willReturn(categoryEntityStream);
		
		Node returnedDto=nodeMapper.createFromEntity(nodeEntity);
		assertThat(returnedDto).isNotNull().hasFieldOrPropertyWithValue("id", nodeEntity.getId());
	}
	
}
