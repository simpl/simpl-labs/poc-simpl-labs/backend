package it.eng.simpl.labs.poc.model;

import java.nio.file.Paths;

import org.springframework.stereotype.Component;

import com.marcnuri.helm.Helm;
import com.marcnuri.helm.InstallCommand;

@Component
public class InstallCommandWrapper {
	
	private InstallCommand installCommand;
	
	public InstallCommand init(String name, String chartDir, String gaChart) {
		
		this.installCommand = new Helm(Paths.get(chartDir + gaChart)).install().withName(name);
		
		return installCommand;
	}
	
	public InstallCommand set(String kubeconfigFile, String namespace) {
		
		installCommand.withKubeConfig(Paths.get((kubeconfigFile))).withNamespace(namespace);
		
		return installCommand;

	}
	
	public InstallCommand install() {
		
		installCommand.waitReady().call();
		
		return installCommand;

	}
	
	public InstallCommand setProperty(String name, Object value) {
		
		installCommand.set(name, value);
		
		return installCommand;

	}

}
