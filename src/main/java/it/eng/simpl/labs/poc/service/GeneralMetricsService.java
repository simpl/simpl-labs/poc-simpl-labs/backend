package it.eng.simpl.labs.poc.service;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.eng.simpl.labs.poc.entity.CurrentMetricsEntity;
import it.eng.simpl.labs.poc.exception.GenericException;
import it.eng.simpl.labs.poc.model.CurrentConsumption;
import it.eng.simpl.labs.poc.model.Dataspace;
import it.eng.simpl.labs.poc.model.HistoricalConsumption;
import it.eng.simpl.labs.poc.model.TenantConsumption;
import it.eng.simpl.labs.poc.model.User;
import it.eng.simpl.labs.poc.repository.CurrentMetricsRepository;
import it.eng.simpl.labs.poc.repository.HistoricalMetricsRepository;
import it.eng.simpl.labs.poc.service.interf.IDataspaceService;
import it.eng.simpl.labs.poc.service.interf.IGeneralMetricsService;
import it.eng.simpl.labs.poc.service.interf.IK8sClusterManagementService;
import it.eng.simpl.labs.poc.utils.Constants;
import it.eng.simpl.labs.poc.utils.GenericUtils;
import lombok.RequiredArgsConstructor;

@Service
@Transactional
@RequiredArgsConstructor
public class GeneralMetricsService implements IGeneralMetricsService {
	
	public static final String THE_LOCAL_DATASPACES_CONFIGURATION_DOESN_T_MATCH_CLUSTER = "The local dataspaces configuration doesn't match cluster";

	private final HistoricalMetricsRepository historicalMetricsRepository;
	
	private final CurrentMetricsRepository currentMetricsRepository;
		
	private final IDataspaceService dataspaceService;
	
	private final IK8sClusterManagementService k8sClusterManagementService; 
	
	@Override
	public List<CurrentConsumption> getCurrentMetrics(User user) {
		
		List<CurrentConsumption> currentDataspacesConsumpionsList = new ArrayList<>();		
		
		List<CurrentMetricsEntity> currentMetricsEntityList = currentMetricsRepository.getGeneralMetrics(user.getUsername());
		
		List<Long> dataspaceIdList = currentMetricsEntityList.stream().map(c -> c.getDataspaceId())
				.distinct()
				.toList();

		for(Long dataspaceId : dataspaceIdList ) {
			
			Dataspace dataspace = dataspaceService.findById(dataspaceId);
			if(dataspace == null) {
				throw new GenericException(THE_LOCAL_DATASPACES_CONFIGURATION_DOESN_T_MATCH_CLUSTER);
			}
			
			BigDecimal cpuUsed = Constants.ZERO;
			BigDecimal cpuAllocated = Constants.ZERO;
			BigDecimal memoryUsed = Constants.ZERO;
			BigDecimal memoryAllocated = Constants.ZERO;
			BigDecimal storageUsed = Constants.ZERO;
			BigDecimal storageAllocated = Constants.ZERO;
			
			for(CurrentMetricsEntity currentMetricsEntity : currentMetricsEntityList) {
				if(currentMetricsEntity.getDataspaceId().equals(dataspaceId)) {
					cpuUsed = cpuUsed.add(currentMetricsEntity.getCpuUsed());
					cpuAllocated = cpuAllocated.add(currentMetricsEntity.getCpuAllocated());
					memoryUsed = memoryUsed.add(currentMetricsEntity.getMemoryUsed());
					memoryAllocated = memoryAllocated.add(currentMetricsEntity.getMemoryAllocated());
					storageUsed = storageUsed.add(currentMetricsEntity.getStorageUsed());
					storageAllocated = storageAllocated.add(currentMetricsEntity.getStorageAllocated());
				}
			}
			
			CurrentConsumption currentDataspacesConsumpions = CurrentConsumption.builder()
					.id(dataspaceId)
					.name(dataspace.getName())
					.status(dataspace.getStatus().toString())
					.cpuUsed(cpuUsed)
					.cpuAllocated(cpuAllocated)
					.memoryUsed(GenericUtils.getGigaBytesFromBytes(memoryUsed))
					.memoryAllocated(GenericUtils.getGigaBytesFromBytes(memoryAllocated))
					.storageAllocated(storageAllocated)
					.storageUsed(storageUsed)
					.build();
			
			currentDataspacesConsumpionsList.add(currentDataspacesConsumpions);
			
		}
						 
		return currentDataspacesConsumpionsList;

	}
	
	@Override
	public TenantConsumption getCurrentCpuMetrics(User user) {	

		List<CurrentMetricsEntity> currentMetricsEntityList = currentMetricsRepository.getGeneralMetrics(user.getUsername());
		
		TenantConsumption tenantConsumption = TenantConsumption.builder()
				.name(Constants.CPU)
				.unitOfMeasurement(Constants.CORES_LOWERCASE)
				.date(LocalDateTime.now().toString())
				.allocatable(k8sClusterManagementService.getAllocatableResources(Constants.CPU_LOWERCASE))
				.build();
		
		for(CurrentMetricsEntity currentMetricsEntity : currentMetricsEntityList) {
			tenantConsumption.setAllocated(tenantConsumption.getAllocated().add(currentMetricsEntity.getCpuAllocated()));
			tenantConsumption.setUsed(tenantConsumption.getUsed().add(currentMetricsEntity.getCpuUsed()));			
		}

		return tenantConsumption;
	}

	@Override
	public TenantConsumption getCurrentMemoryMetrics(User user) {
		
		List<CurrentMetricsEntity> currentMetricsEntityList = currentMetricsRepository.getGeneralMetrics(user.getUsername());
		
		BigDecimal allocatableAmount = GenericUtils.getGigaBytesFromBytes(k8sClusterManagementService.getAllocatableResources(Constants.MEMORY_LOWERCASE));
		
		TenantConsumption tenantConsumption = TenantConsumption.builder()
				.name(Constants.MEMORY)
				.unitOfMeasurement(Constants.GIGABYTES)
				.date(LocalDateTime.now().toString())
				.allocatable(allocatableAmount)
				.build();
		
		for(CurrentMetricsEntity currentMetricsEntity : currentMetricsEntityList) {
			tenantConsumption.setAllocated(tenantConsumption.getAllocated().add(currentMetricsEntity.getMemoryAllocated()));
			tenantConsumption.setUsed(tenantConsumption.getUsed().add(currentMetricsEntity.getMemoryUsed()));			
		}
		
		tenantConsumption.setAllocated(GenericUtils.getGigaBytesFromBytes(tenantConsumption.getAllocated()));
		tenantConsumption.setUsed(GenericUtils.getGigaBytesFromBytes(tenantConsumption.getUsed()));

		return tenantConsumption;
	}

	@Override
	public TenantConsumption getCurrentStorageMetrics(User user) {
		
		List<CurrentMetricsEntity> currentMetricsEntityList = currentMetricsRepository.getGeneralMetrics(user.getUsername());
		
		TenantConsumption tenantConsumption = TenantConsumption.builder()
				.name(Constants.STORAGE)
				.unitOfMeasurement(Constants.GIGABYTES)
				.date(LocalDateTime.now().toString())
				.allocatable(k8sClusterManagementService.getAllocatableResources(Constants.STORAGE_LOWERCASE))
				.build();
		
		for(CurrentMetricsEntity currentMetricsEntity : currentMetricsEntityList) {
			tenantConsumption.setAllocated(tenantConsumption.getAllocated().add(currentMetricsEntity.getStorageAllocated()));
			tenantConsumption.setUsed(tenantConsumption.getUsed().add(currentMetricsEntity.getStorageUsed()));			
		}

		return tenantConsumption;
	}
	
	
	@Override
	public List<HistoricalConsumption> getHistoricalCpuMetrics(LocalDate startDate, LocalDate endDate, User user) {

		List<Object[]> list = historicalMetricsRepository.getGeneralHistoricalMetrics(startDate, endDate, user.getUsername());
				
		return GenericUtils.getModelFromObjectsList(list, Constants.CPU_INDEX);
		
	}


	@Override
	public List<HistoricalConsumption> getHistoricalMemoryMetrics(LocalDate startDate, LocalDate endDate, User user) {

		List<Object[]> list = historicalMetricsRepository.getGeneralHistoricalMetrics(startDate, endDate, user.getUsername());
		
		List<HistoricalConsumption> historicalDataspacesConsumptionList = GenericUtils.getModelFromObjectsList(list, Constants.MEMORY_INDEX);
		
		for(HistoricalConsumption historicalDataspacesConsumption : historicalDataspacesConsumptionList) {
			historicalDataspacesConsumption.setAllocated(GenericUtils.getGigaBytesFromBytes(historicalDataspacesConsumption.getAllocated()));
			historicalDataspacesConsumption.setUsed(GenericUtils.getGigaBytesFromBytes(historicalDataspacesConsumption.getUsed()));
		}
		
		return historicalDataspacesConsumptionList;
		
	}

	@Override
	public List<HistoricalConsumption> getHistoricalStorageMetrics(LocalDate startDate, LocalDate endDate, User user) {

		List<Object[]> list = historicalMetricsRepository.getGeneralHistoricalMetrics(startDate, endDate, user.getUsername());
		
		return GenericUtils.getModelFromObjectsList(list, Constants.STORAGE_INDEX);
		
	}
	
}
