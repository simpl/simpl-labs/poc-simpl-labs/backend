package it.eng.simpl.labs.poc.repository;

import java.util.List;

import org.springframework.stereotype.Repository;

import it.eng.simpl.labs.poc.entity.TestSuiteEntity;

@Repository
public interface TestSuiteRepository extends BaseRepository<TestSuiteEntity, String> {
	
	public List<TestSuiteEntity> findBySpecificationId(Long specificationId);
	
	public List<TestSuiteEntity> findByIdIn(List<String> testSuiteIdList);

}
