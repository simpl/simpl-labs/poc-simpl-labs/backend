package it.eng.simpl.labs.poc.model;

import java.util.List;

import it.eng.simpl.labs.poc.enumeration.CategoryTypeEnum;
import lombok.Builder;
import lombok.Data;
import lombok.ToString;

@Data
@ToString
@Builder
public class Category {

	private Long id;
	
	private CategoryTypeEnum type;
	
	private List<Component> componentList;
	
	private String podIdentifier;

}
