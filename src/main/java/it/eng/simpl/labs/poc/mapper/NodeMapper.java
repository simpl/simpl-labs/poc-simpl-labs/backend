package it.eng.simpl.labs.poc.mapper;

import java.util.List;

import org.springframework.stereotype.Component;

import it.eng.simpl.labs.poc.entity.CategoryEntity;
import it.eng.simpl.labs.poc.entity.NodeEntity;
import it.eng.simpl.labs.poc.model.Node;
import lombok.AllArgsConstructor;

@Component
@AllArgsConstructor
public class NodeMapper extends BaseMapper<Node, NodeEntity> {

	private final CategoryMapper categoryMapper;

	@Override
	public NodeEntity createFromDto(Node dto) {

		NodeEntity entity = new NodeEntity();

		copyProperties(dto, entity);

		List<CategoryEntity> categoryList = categoryMapper.createFromDtos(dto.getCategoryList()).toList();
		categoryList.forEach(category -> category.setNode(entity));
		entity.setCategoryList(categoryList);

		return entity;
	}

	@Override
	public Node createFromEntity(NodeEntity entity) {
		
		Node dto = Node.builder().build();
		
		copyProperties(entity, dto);
	
		dto.setCategoryList(categoryMapper.createFromEntities(entity.getCategoryList()).toList());
		
		return dto;
		
	}

}
