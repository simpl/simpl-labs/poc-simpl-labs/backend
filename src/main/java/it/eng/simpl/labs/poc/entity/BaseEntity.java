package it.eng.simpl.labs.poc.entity;

import java.time.LocalDateTime;

import jakarta.persistence.Column;
import jakarta.persistence.MappedSuperclass;
import jakarta.persistence.PrePersist;
import jakarta.persistence.PreUpdate;
import lombok.Getter;
import lombok.Setter;

/**
 * 
 */
@MappedSuperclass
@Getter
@Setter
public class BaseEntity {
	
	@Column(name = "create_date", updatable = false, columnDefinition = "TIMESTAMP")
	private LocalDateTime createDate;
	
	@Column(name = "last_update_date", columnDefinition = "TIMESTAMP")
	private LocalDateTime lastUpdateDate;
	
	@Column(name = "user_id", updatable = false)
	private String userId;
	
	@PrePersist
	private void baseEntityPrePersist() {
		LocalDateTime now = LocalDateTime.now();
		this.setCreateDate(now);
		this.setLastUpdateDate(now);
	}
	
	@PreUpdate
	private void baseEntityPreUpdate() {
		this.setLastUpdateDate(LocalDateTime.now());
	}

}
