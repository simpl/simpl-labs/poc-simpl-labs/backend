package it.eng.simpl.labs.poc.model;

import java.util.List;

import it.eng.simpl.labs.poc.enumeration.NodeTypeEnum;
import lombok.Builder;
import lombok.Data;
import lombok.ToString;

@Data
@ToString
@Builder
public class Node {
	private Long id;
	private String name;
	private NodeTypeEnum type;
	private List<Category> categoryList;
}
