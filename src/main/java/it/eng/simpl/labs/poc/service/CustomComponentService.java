package it.eng.simpl.labs.poc.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.eng.simpl.labs.poc.entity.CustomComponentEntity;
import it.eng.simpl.labs.poc.mapper.CustomComponentMapper;
import it.eng.simpl.labs.poc.model.Category;
import it.eng.simpl.labs.poc.model.Component;
import it.eng.simpl.labs.poc.model.CustomComponent;
import it.eng.simpl.labs.poc.model.Dataspace;
import it.eng.simpl.labs.poc.model.Node;
import it.eng.simpl.labs.poc.model.User;
import it.eng.simpl.labs.poc.repository.CustomComponentRepository;
import it.eng.simpl.labs.poc.service.interf.ICustomComponentService;
import it.eng.simpl.labs.poc.service.interf.IDataspaceService;
import lombok.RequiredArgsConstructor;

@Service
@Transactional
@RequiredArgsConstructor
public class CustomComponentService implements ICustomComponentService{

	private final CustomComponentRepository customComponentRepository;

	private final CustomComponentMapper customComponentMapper;

	private final IDataspaceService dataspaceService;

	@Override
	public List<CustomComponent> findAll() {

		List<CustomComponentEntity> customComponentEntityList = customComponentRepository.findAll();

		return customComponentMapper.createFromEntities(customComponentEntityList).toList();
	}

	@Override
	public List<Dataspace> getDataspaces(Long componentId, User user) {

		List<Dataspace> componentDataspaceList = new ArrayList<>();

		List<Dataspace> dataspaceList = dataspaceService.findAll(user);
		if(dataspaceList != null) {
			for(Dataspace dataspace : dataspaceList) {
				for(Node node : dataspace.getNodeList()) {
					for(Category category : node.getCategoryList()) {
						for(Component component : category.getComponentList()) {
							if((componentId.equals(component.getId()) || componentId.equals(component.getCustomComponentId())) && (!componentDataspaceList.contains(dataspace))) {
								componentDataspaceList.add(dataspace);
								break;
							}
						}
					}
				}
			}
		}

		return componentDataspaceList;
	}

	@Override
	public CustomComponent findById(Long id) {

		CustomComponentEntity customComponentEntity = customComponentRepository.findById(id).orElse(null);

		return customComponentMapper.createFromEntity(customComponentEntity);
	}

}
