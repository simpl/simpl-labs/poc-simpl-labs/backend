package it.eng.simpl.labs.poc.controller;

import java.time.LocalDate;
import java.util.List;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.MediaType;
import org.springframework.security.core.annotation.CurrentSecurityContext;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import it.eng.simpl.labs.poc.model.CurrentConsumption;
import it.eng.simpl.labs.poc.model.GenericResponse;
import it.eng.simpl.labs.poc.model.HistoricalConsumption;
import it.eng.simpl.labs.poc.model.TenantConsumption;
import it.eng.simpl.labs.poc.model.User;
import it.eng.simpl.labs.poc.service.interf.IGeneralMetricsService;
import lombok.RequiredArgsConstructor;

@Tag(name = "System Monitoring", description = "System Monitoring management APIs")
@RestController
@RequestMapping("/system-monitoring")
@RequiredArgsConstructor
public class GeneralMonitoringController {
	
	private final IGeneralMetricsService componentMetricsService;
	
	@GetMapping("/general/current")
	@SecurityRequirement(name = "Authentication")
	@Operation(summary = "Get the general current resources usage")
	@ApiResponse(responseCode = "200", description = "Action successfully executed", content = {
			@Content(mediaType = MediaType.APPLICATION_JSON_VALUE, array = @ArraySchema(schema = @Schema(implementation = CurrentConsumption.class))) })
	@ApiResponse(responseCode = "500", description = "Error executing action", content = {
			@Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = GenericResponse.class)) })
	public List<CurrentConsumption> getDataspacesCurrent(
			@CurrentSecurityContext(expression = "authentication.user") @Parameter(description = "The current user") User user ) {

		return componentMetricsService.getCurrentMetrics(user);

	}

	@GetMapping("/general/current/cpu")
	@SecurityRequirement(name = "Authentication")
	@Operation(summary = "Get the general current CPU usage")
	@ApiResponse(responseCode = "200", description = "Action successfully executed", content = {
			@Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = TenantConsumption.class)) })
	@ApiResponse(responseCode = "500", description = "Error executing action", content = {
			@Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = GenericResponse.class)) })
	public TenantConsumption getDataspacesCurrentCpu(
			@CurrentSecurityContext(expression = "authentication.user") @Parameter(description = "The current user") User user) {

		return componentMetricsService.getCurrentCpuMetrics(user);

	}
	
	@GetMapping("/general/current/memory")
	@SecurityRequirement(name = "Authentication")
	@Operation(summary = "Get the general current MEMORY usage")
	@ApiResponse(responseCode = "200", description = "Action successfully executed", content = {
			@Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = TenantConsumption.class)) })
	@ApiResponse(responseCode = "500", description = "Error executing action", content = {
			@Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = GenericResponse.class)) })
	public TenantConsumption getDataspacesCurrentMemory(
			@CurrentSecurityContext(expression = "authentication.user") @Parameter(description = "The current user") User user) {

		return componentMetricsService.getCurrentMemoryMetrics(user);

	}
	
	@GetMapping("/general/current/storage")
	@SecurityRequirement(name = "Authentication")
	@Operation(summary = "Get the general current STORAGE usage")
	@ApiResponse(responseCode = "200", description = "Action successfully executed", content = {
			@Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = TenantConsumption.class)) })
	@ApiResponse(responseCode = "500", description = "Error executing action", content = {
			@Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = GenericResponse.class)) })
	public TenantConsumption getDataspacesCurrentStorage(
			@CurrentSecurityContext(expression = "authentication.user") @Parameter(description = "The current user") User user ) {

		return componentMetricsService.getCurrentStorageMetrics(user);

	}
	
	@GetMapping("/general/historical/cpu/{startDate}/{endDate}")
	@SecurityRequirement(name = "Authentication")
	@Operation(summary = "Get the general historical CPU usage")
	@ApiResponse(responseCode = "200", description = "Action successfully executed", content = {
			@Content(mediaType = MediaType.APPLICATION_JSON_VALUE, array = @ArraySchema(schema = @Schema(implementation = HistoricalConsumption.class))) })
	@ApiResponse(responseCode = "500", description = "Error executing action", content = {
			@Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = GenericResponse.class)) })
	public List<HistoricalConsumption> getDataspacesHistoricalCpu(
			@Parameter @PathVariable("startDate")  @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate startDate,
			@Parameter @PathVariable("endDate") @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate endDate,
			@CurrentSecurityContext(expression = "authentication.user") @Parameter(description = "The current user") User user ) {

		return componentMetricsService.getHistoricalCpuMetrics(startDate, endDate, user);

	}
	
	@GetMapping("/general/historical/memory/{startDate}/{endDate}")
	@SecurityRequirement(name = "Authentication")
	@Operation(summary = "Get the general historical Memory usage")
	@ApiResponse(responseCode = "200", description = "Action successfully executed", content = {
			@Content(mediaType = MediaType.APPLICATION_JSON_VALUE, array = @ArraySchema(schema = @Schema(implementation = HistoricalConsumption.class))) })
	@ApiResponse(responseCode = "500", description = "Error executing action", content = {
			@Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = GenericResponse.class)) })
	public List<HistoricalConsumption> getDataspacesHistoricalMemory(
			@Parameter @PathVariable("startDate")  @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate startDate,
			@Parameter @PathVariable("endDate") @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate endDate,
			@CurrentSecurityContext(expression = "authentication.user") @Parameter(description = "The current user") User user ) {

		return componentMetricsService.getHistoricalMemoryMetrics(startDate, endDate, user);

	}
	
	@GetMapping("/general/historical/storage/{startDate}/{endDate}")
	@SecurityRequirement(name = "Authentication")
	@Operation(summary = "Get the general historical Storage usage")
	@ApiResponse(responseCode = "200", description = "Action successfully executed", content = {
			@Content(mediaType = MediaType.APPLICATION_JSON_VALUE, array = @ArraySchema(schema = @Schema(implementation = HistoricalConsumption.class))) })
	@ApiResponse(responseCode = "500", description = "Error executing action", content = {
			@Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = GenericResponse.class)) })
	public List<HistoricalConsumption> getDataspacesHistoricalStorage(
			@Parameter @PathVariable("startDate")  @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate startDate,
			@Parameter @PathVariable("endDate") @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate endDate,
			@CurrentSecurityContext(expression = "authentication.user") @Parameter(description = "The current user") User user ) {

		return componentMetricsService.getHistoricalStorageMetrics(startDate, endDate, user);

	}

}
