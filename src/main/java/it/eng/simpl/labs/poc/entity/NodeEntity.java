package it.eng.simpl.labs.poc.entity;

import java.util.List;

import it.eng.simpl.labs.poc.enumeration.NodeTypeEnum;
import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Enumerated;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToMany;
import jakarta.validation.constraints.Max;
import jakarta.validation.constraints.Min;
import lombok.Getter;
import lombok.Setter;

@Entity()
@Getter
@Setter
public class NodeEntity extends BaseEntity {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column
	@Min(0)
	@Max(9223372036854775807l)
	private Long id;
	
	@Column
	private String name;
	
	@Column
	@Enumerated
	private NodeTypeEnum type;
	
	@ManyToOne
	@JoinColumn(name="dataspace_id", nullable=false)
	private DataspaceEntity dataspace;
	
	@OneToMany(mappedBy = "node", cascade = CascadeType.ALL)
	private List<CategoryEntity> categoryList;

}
