package it.eng.simpl.labs.poc.scheduler;

import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import it.eng.simpl.labs.poc.service.interf.ITestSessionsStatusUpdateService;
import lombok.AllArgsConstructor;

@Component
@AllArgsConstructor
public class TestExecutionUpdateStatusScheduler {

	private final ITestSessionsStatusUpdateService testSessionsStatusUpdateService;

	@Scheduled(fixedRateString = "${itb.scheduler.fixedRate}")
	public void run() {

		testSessionsStatusUpdateService.updateTestExecutionStatus();

	}

}
