package it.eng.simpl.labs.poc.service;

import java.util.List;

import org.apache.commons.lang3.exception.ExceptionUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.eng.simpl.labs.poc.entity.CategoryEntity;
import it.eng.simpl.labs.poc.entity.DataspaceEntity;
import it.eng.simpl.labs.poc.enumeration.ComponentTypeEnum;
import it.eng.simpl.labs.poc.enumeration.DataspaceStatusEnum;
import it.eng.simpl.labs.poc.exception.GenericException;
import it.eng.simpl.labs.poc.model.Category;
import it.eng.simpl.labs.poc.model.Component;
import it.eng.simpl.labs.poc.model.Dataspace;
import it.eng.simpl.labs.poc.model.InstallCommandWrapper;
import it.eng.simpl.labs.poc.model.Node;
import it.eng.simpl.labs.poc.model.UninstallCommandWrapper;
import it.eng.simpl.labs.poc.repository.CategoryRepository;
import it.eng.simpl.labs.poc.repository.ComponentRepository;
import it.eng.simpl.labs.poc.repository.DataspaceRepository;
import it.eng.simpl.labs.poc.service.interf.IDeploymentService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Service
@Transactional
@Slf4j
@RequiredArgsConstructor
public class DeploymentService implements IDeploymentService {

	@Value("${deployment.kubeconfig}")
	private String kubeconfigFile;

	@Value("${deployment.chartDir}")
	public String chartDir;

	@Value("${deployment.namespace}")
	private String namespace;

	@Value("${deployment.gaChart}")
	private String gaChart;

	@Value("${deployment.dataChart}")
	private String dataChart;

	@Value("${deployment.appChart}")
	private String appChart;

	@Value("${deployment.infraChart}")
	private String infraChart;

	@Value("${deployment.consumerChart}")
	private String consumerChart;

	private final DataspaceRepository dataspaceRepository;

	private final CategoryRepository categoryRepository;

	private final ComponentRepository componentRepository;

	private final InstallCommandWrapper installCommand;

	private final UninstallCommandWrapper uninstallCommand;
	
	private long sleepTime = 5000L;

	@Override
	@Async
	public void initDataspaceCreation(Dataspace dataspace) {

		DataspaceStatusEnum status = null;

		DataspaceEntity entity = null;

		try {

			log.info("Async dataspace creation");

			Thread.sleep(sleepTime);

			entity = dataspaceRepository.findById(dataspace.getId()).get();

			log.info("Init dataspace {} creation",dataspace.getId());

			entity.setStatus(DataspaceStatusEnum.INSTALLING);

			for (Node node : dataspace.getNodeList()) {

				//Setup chart

				switch (node.getType()) {
				case GOVERNANCE: {					
					installCommand.init(DATASPACE_PREFIX+dataspace.getId()+GA+node.getId(), chartDir, gaChart);
					break;
				}
				case APPLICATION_PROVIDER: {
					installCommand.init(DATASPACE_PREFIX+dataspace.getId()+APP+node.getId(), chartDir, appChart);
					break;
				}
				case DATA_PROVIDER: {
					installCommand.init(DATASPACE_PREFIX+dataspace.getId()+DATA+node.getId(), chartDir, dataChart);
					break;
				}
				case INFRASTRUCTURE_PROVIDER: {
					installCommand.init(DATASPACE_PREFIX+dataspace.getId()+INFRA+node.getId(), chartDir, infraChart);
					break;
				}
				case CONSUMER: {
					installCommand.init(DATASPACE_PREFIX+dataspace.getId()+CONSUMER+node.getId(), chartDir, consumerChart);
					break;
				}
				}

				//Default settings
				installCommand.set(kubeconfigFile, namespace);

				//Setup each component inside each category
				customizeCategory(dataspace,node);

				//Install helm chart
				installCommand.install();
			}
			log.info("Invoked helm charts for dataspace {}",dataspace.getId());
			status=DataspaceStatusEnum.RUNNING;
		} catch (InterruptedException e) { 
			log.error("Interrupted!", e);
			Thread.currentThread().interrupt();
		} catch (Exception e) {
			log.error(ExceptionUtils.getStackTrace(e));			
			status=DataspaceStatusEnum.ERROR;
		}

		//Update dataspace status
		if (entity!=null) {
			entity.setStatus(status);
			dataspaceRepository.save(entity);
			log.info("Updated dataspace {} with status {}",dataspace.getId(),status);
		}
	}

	/**
	 * Customize category params for Helm chart installation
	 * 
	 * @param dataspace
	 * @param node
	 * @param installCommand
	 */
	private void customizeCategory(Dataspace dataspace, Node node) {
		
		for (Category category : node.getCategoryList()) {
			
			CategoryEntity categoryEntity = categoryRepository.findByNodeIdAndType(node.getId(), category.getType());
			
			if(categoryEntity != null) {
				String podIdentifier = dataspace.getId() + "-" + node.getId() + "-" + category.getId();
				installCommand.setProperty(category.getType().getPrefix()+"PodIdentifier", podIdentifier);
				category.setPodIdentifier(podIdentifier);
				categoryEntity.setPodIdentifier(podIdentifier);
				categoryRepository.saveAndFlush(categoryEntity);
			} else {
				throw new GenericException("Uncorrect data base configuration.");
			}

			List<Component> componentList = category.getComponentList();
			customizeComponent(category, componentList);
		}
	}

	/**
	 * Customize component params for Helm chart installation
	 * 
	 * @param installCommand
	 * @param category
	 * @param componentList
	 */
	private void customizeComponent(Category category, List<Component> componentList) {
		for (Component component : componentList) {
			String identifier = buildComponentIdentifier(category, component);
			if (component.getCustomImage() != null) {
				installCommand.setProperty(identifier + ".image", component.getCustomImage().split(":")[0]);
				installCommand.setProperty(identifier + ".imageTag",  component.getCustomImage().split(":")[1]);
			}
			installCommand.setProperty(identifier+PARAM1, component.getParam1());
			installCommand.setProperty(identifier+PARAM2, component.getParam2());
			installCommand.setProperty(identifier+PARAM3, component.getParam3());
			installCommand.setProperty(identifier+PARAM4, component.getParam4());
			installCommand.setProperty(identifier+PARAM5, component.getParam5());
			installCommand.setProperty(identifier+PARAM6, component.getParam6());
		}
	}

	/**
	 * Build identifier for component inside Helm values
	 * 
	 * @param category
	 * @param component
	 * @return
	 */
	private String buildComponentIdentifier(Category category, Component component) {
		StringBuilder sb = new StringBuilder();
		sb.append(category.getType().getPrefix());
		if (ComponentTypeEnum.BE_COMPONENT.equals(component.getType()))
			sb.append(BE);
		else
			sb.append(FE);
		return sb.toString();
	}
	
	/**
	 * Asynchronous deletion of a data space
	 * @param dataspaceToDelete
	 */
	@Async
	@Override
	public void initDataspaceDeletion(Dataspace dataspaceToDelete) {

		log.info("Init dataspace {} deletion", dataspaceToDelete.getId());

		DataspaceStatusEnum status=null;
		DataspaceEntity entity=null;
		try {
			log.info("Async dataspace deletion");
			Thread.sleep(sleepTime);
			entity = dataspaceRepository.findById(dataspaceToDelete.getId()).get();
			log.info("Init dataspace {} deletion",dataspaceToDelete.getId());
			entity.setStatus(DataspaceStatusEnum.DELETING);
		} catch (InterruptedException e) { 
			log.error("Interrupted!", e);
			Thread.currentThread().interrupt();
		} catch (Exception e) {
			log.error(ExceptionUtils.getStackTrace(e));		
			status=DataspaceStatusEnum.ERROR;
		}

		//Update dataspace status
		if (entity!=null) {

			entity.setStatus(status);
			dataspaceRepository.save(entity);
			log.info("Updated dataspace {} with status {}",dataspaceToDelete.getId(),status);

			for(Node node : dataspaceToDelete.getNodeList()) {

				//Setup chart
				switch (node.getType()) {
				case GOVERNANCE: {
					uninstallCommand.init(DATASPACE_PREFIX+dataspaceToDelete.getId()+GA+node.getId());
					break;
				}
				case APPLICATION_PROVIDER: {
					uninstallCommand.init(DATASPACE_PREFIX+dataspaceToDelete.getId()+APP+node.getId());
					break;
				}
				case DATA_PROVIDER: {
					uninstallCommand.init(DATASPACE_PREFIX+dataspaceToDelete.getId()+DATA+node.getId());
					break;
				}
				case INFRASTRUCTURE_PROVIDER: {
					uninstallCommand.init(DATASPACE_PREFIX+dataspaceToDelete.getId()+INFRA+node.getId());
					break;
				}
				case CONSUMER: {
					uninstallCommand.init(DATASPACE_PREFIX+dataspaceToDelete.getId()+CONSUMER+node.getId());
					break;
				}
				}
				
				//Default settings
				uninstallCommand.set(kubeconfigFile, namespace);

				//Delete helm release
				log.info("Delete Helm charts for dataspace {} ", dataspaceToDelete.getId());
				uninstallCommand.uninstall();

				// Delete components
				for(Category category : node.getCategoryList()) {
					for(Component component : category.getComponentList()) {
						log.info("Delete Components data for dataspace {} - component {}", dataspaceToDelete.getId(), component.getId());
						componentRepository.deleteById(component.getId());
						componentRepository.flush();
					}
				}

			}

			// Delete dataspace
			log.info("Delete dataspace {} ", dataspaceToDelete.getId());
			dataspaceRepository.delete(entity);
			dataspaceRepository.flush();

			log.info("Removed dataspace {}", dataspaceToDelete.getId());

		} else {
			log.error("No dataspace to delete found with id {}",dataspaceToDelete.getId());		
		}
	}
}
