package it.eng.simpl.labs.poc.exception;

public class GenericException extends RuntimeException {

	private static final long serialVersionUID = -4978490348961874169L;

	public GenericException(String message) {
		super(message);
	}

	public GenericException() {
		super();
	}
	
	public GenericException(Throwable e) {
		super(e);
	}

	public GenericException(String message, Throwable e) {
		super(message, e);
	}
}