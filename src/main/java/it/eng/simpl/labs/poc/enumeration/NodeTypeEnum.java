package it.eng.simpl.labs.poc.enumeration;

import lombok.ToString;

@ToString
public enum NodeTypeEnum {
	GOVERNANCE,
	DATA_PROVIDER,
	APPLICATION_PROVIDER,
	INFRASTRUCTURE_PROVIDER,
	CONSUMER;
}
