package it.eng.simpl.labs.poc.entity;

import java.math.BigDecimal;

import jakarta.persistence.Column;
import jakarta.persistence.MappedSuperclass;
import lombok.Getter;
import lombok.Setter;

@MappedSuperclass
@Getter
@Setter
public class BaseMetricsEntity extends BaseEntity {
	
	@Column(nullable = false)
	private Long dataspaceId;
	
	@Column(nullable = false)
	private Long nodeId;
	
	@Column(nullable = false)
	private Long categoryId;
	
	@Column(name = "component_id", nullable = false)
	private Long componentId;
	
	@Column(precision = 20, scale = 5)
	private BigDecimal cpuAllocated;
	
	@Column(precision = 20, scale = 5)
	private BigDecimal cpuUsed;
	
	@Column
	private BigDecimal memoryAllocated;
	
	@Column
	private BigDecimal memoryUsed;
	
	@Column
	private BigDecimal storageAllocated;
	
	@Column
	private BigDecimal storageUsed;

}
