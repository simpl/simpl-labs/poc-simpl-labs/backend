package it.eng.simpl.labs.poc.entity;

import jakarta.persistence.Column;
import jakarta.persistence.Embeddable;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Embeddable
public class TestCaseEntityPK {

    @Column(name = "test_case_id")
    private String testCaseId;

    @Column(name = "test_suite_id")
    private String testSuiteId;
}
