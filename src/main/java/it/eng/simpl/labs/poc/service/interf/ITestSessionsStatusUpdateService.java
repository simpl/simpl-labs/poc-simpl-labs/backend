package it.eng.simpl.labs.poc.service.interf;

public interface ITestSessionsStatusUpdateService {

	/**
	 * 
	 */
	public void updateTestExecutionStatus();
	
}
