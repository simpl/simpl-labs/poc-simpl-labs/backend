package it.eng.simpl.labs.poc.model;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

@Data
@AllArgsConstructor
@Builder
public class GenericResponse {
	
	@Schema(name = "code", description = "The action execution code")
	private Integer code;

	@Schema(name = "message", description = "The message")
	private String message;
	
}
