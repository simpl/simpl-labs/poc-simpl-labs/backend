package it.eng.simpl.labs.poc.model.itb;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Builder;
import lombok.Data;

/**
 * Information on the stopped test sessions.
 */
@Data
@Builder
public class SessionStopResponse implements ISessionResponse {

	@Schema(name = "executionProgressive", description = "The tests session execution progressive")
	private String executionProgressive;

}
