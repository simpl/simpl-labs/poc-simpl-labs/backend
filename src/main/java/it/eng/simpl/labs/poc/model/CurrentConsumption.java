package it.eng.simpl.labs.poc.model;

import java.math.BigDecimal;

import io.swagger.v3.oas.annotations.media.Schema;
import it.eng.simpl.labs.poc.utils.Constants;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class CurrentConsumption {
	
	@Schema(name = "id", description = "The Dataspace id")
	private Long id;

	@Schema(name = "name", description = "The Dataspace name")
	private String name;
	
	@Schema(name = "componentId", description = "The Component id")
	private Long componentId;

	@Schema(name = "componentName", description = "The Component name")
	private String componentName;
	
	@Schema(name = "nodeId", description = "The Node id")
	private Long nodeId;
	
	@Schema(name = "nodeType", description = "The Node type")
	private String nodeType;

	@Schema(name = "nodeName", description = "The Node name")
	private String nodeName;
	
	@Schema(name = "categoryId", description = "The Category id")
	private Long categoryId;

	@Schema(name = "categoryName", description = "The Category name")
	private String categoryName;
	
	@Schema(name = "status", description = "The Dataspace status")
	private String status;

	@Builder.Default
	@Schema(name = "cpuUsed", description = "The Dataspace used CPU")
	private BigDecimal cpuUsed = Constants.ZERO;
	
	@Builder.Default
	@Schema(name = "cpuAllocated", description = "The Dataspace allocated CPU")
	private BigDecimal cpuAllocated = Constants.ZERO;
	
	@Builder.Default
	@Schema(name = "memoryUsed", description = "The Dataspace used Memory")
	private BigDecimal memoryUsed = Constants.ZERO;
	
	@Builder.Default
	@Schema(name = "memoryAllocated", description = "The Dataspace allocated Memory")
	private BigDecimal memoryAllocated = Constants.ZERO;

	@Builder.Default
	@Schema(name = "storageUsed", description = "The Dataspace used Storage")
	private BigDecimal storageUsed = Constants.ZERO;
	
	@Builder.Default
	@Schema(name = "storageAllocated", description = "The Dataspace allocated Storage")
	private BigDecimal storageAllocated = Constants.ZERO;
}
