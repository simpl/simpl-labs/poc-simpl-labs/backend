/**
 * 
 */
package it.eng.simpl.labs.poc.service.interf;

import java.math.BigDecimal;
import java.util.List;

import it.eng.simpl.labs.poc.entity.CurrentMetricsEntity;
import it.eng.simpl.labs.poc.entity.HistoricalMetricsEntity;

/**
 * 
 */
public interface IK8sClusterManagementService {
	
	public static final String MEMORY = "memory";

	public static final String CPU = "cpu";

	public static final String FE_COMPONENT = "fe-component";

	public static final String BE_COMPONENT = "be-component";

	public static final String POD_IDENTIFIER = "pod-identifier";
	
	public static final String STORAGE = "storage";

	/**
	 * Get Metrics from Kubernetes cluster 
	 */
	public List<HistoricalMetricsEntity> getHistoricalMetrics();
	
	/**
	 * Get Metrics from Kubernetes cluster 
	 */
	public List<CurrentMetricsEntity> getCurrentMetrics();
	
	/**
	 * Get allocatable @resourceType resource
	 * 
	 * @param resourceType
	 * @return
	 */
	public BigDecimal getAllocatableResources(String resourceType);
	
}
