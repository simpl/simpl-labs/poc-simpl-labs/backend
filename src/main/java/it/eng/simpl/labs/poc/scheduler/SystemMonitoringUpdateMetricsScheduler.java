package it.eng.simpl.labs.poc.scheduler;

import java.time.LocalDateTime;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import it.eng.simpl.labs.poc.service.interf.IMetricsManagementService;
import lombok.RequiredArgsConstructor;

@Component
@RequiredArgsConstructor
public class SystemMonitoringUpdateMetricsScheduler {

	private final IMetricsManagementService metricsManagementService;
	
	@Value("${system.monitoring.historical.window.size}")
	private String historicalWindowSize;

	@Scheduled(fixedRateString = "${system.monitoring.scheduler.fixedRate}")
	public void run() {
		
		metricsManagementService.deleteOrphans();

		metricsManagementService.deleteOldMetrics(LocalDateTime.now().minusDays(Integer.parseInt(historicalWindowSize)));

		metricsManagementService.updateMetrics();
	
	}
	
	@Scheduled(fixedRateString = "${system.monitoring.scheduler.buffer.fixedRate}")
	public void runBufferedMetrics() {
		
		metricsManagementService.updateBufferedMetrics();
	
	}
	
}
