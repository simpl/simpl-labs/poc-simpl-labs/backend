package it.eng.simpl.labs.poc.controller;

import org.springframework.http.MediaType;
import org.springframework.security.core.annotation.CurrentSecurityContext;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import it.eng.simpl.labs.poc.model.GenericResponse;
import it.eng.simpl.labs.poc.model.TestCaseExecutionLogs;
import it.eng.simpl.labs.poc.model.TestCaseExecutionReport;
import it.eng.simpl.labs.poc.model.TestSessionStartRequest;
import it.eng.simpl.labs.poc.model.TestSessionStartResponse;
import it.eng.simpl.labs.poc.model.TestSessionStatus;
import it.eng.simpl.labs.poc.model.User;
import it.eng.simpl.labs.poc.model.itb.SessionStartResponse;
import it.eng.simpl.labs.poc.model.itb.SessionStopRequest;
import it.eng.simpl.labs.poc.model.itb.SessionStopResponse;
import it.eng.simpl.labs.poc.service.interf.ITestExecutionService;
import it.eng.simpl.labs.poc.service.interf.ITestSessionsManagementService;
import lombok.RequiredArgsConstructor;

@Tag(name = "Custom Components Tests", description = "Tests related to Custom Components")
@RestController
@RequestMapping("/custom-components/tests")
@RequiredArgsConstructor
public class TestController {
	
	private final ITestSessionsManagementService testSessionsManagementService;

	private final ITestExecutionService testExecutionService;
	
	@PostMapping("/start")
	@SecurityRequirement(name = "Authentication")
	@Operation(summary = "Start the test execution")
	@ApiResponse(responseCode = "200", description = "Action successfully executed", content = {
			@Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = SessionStartResponse.class)) })
	@ApiResponse(responseCode = "500", description = "Error executing action", content = {
			@Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = GenericResponse.class)) })	
	public TestSessionStartResponse startsTestsSession(@RequestBody TestSessionStartRequest sessionStartRequest,
			@Parameter(hidden = true) @CurrentSecurityContext(expression = "authentication.user") User user) {
		
		return testSessionsManagementService.startTestSession(sessionStartRequest);

	}

	@PostMapping("/stop")
	@SecurityRequirement(name = "Authentication")
	@Operation(summary = "Start the test execution")
	@ApiResponse(responseCode = "200", description = "Action successfully executed", content = {
			@Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = SessionStopResponse.class)) })
	@ApiResponse(responseCode = "500", description = "Error executing action", content = {
			@Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = GenericResponse.class)) })	
	public SessionStopResponse stopTestsSession(@RequestBody SessionStopRequest sessionStopRequest,
			@Parameter(hidden = true) @CurrentSecurityContext(expression = "authentication.user") User user) {

		return testSessionsManagementService.stopTestSession(sessionStopRequest);

	}
	
	@GetMapping("/status/{specificationId}/{componentId}")
	@SecurityRequirement(name = "Authentication")
	@Operation(summary = "Get the tests executions status")
	@ApiResponse(responseCode = "200", description = "Action successfully executed", content = {
			@Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = TestSessionStatus.class)) })
	@ApiResponse(responseCode = "500", description = "Error executing action", content = {
			@Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = GenericResponse.class)) })	
	public TestSessionStatus getTestsExecutionStatus(
			@CurrentSecurityContext(expression = "authentication.user") @Parameter(description = "The current user") User user,
			@PathVariable @Parameter(description = "The specification id") Long specificationId,
			@PathVariable @Parameter(description = "The component id") Long componentId) {
		
		return testExecutionService.getTestsExecutionStatus(specificationId, componentId);

	}
	
	@GetMapping("/report/{testSessionId}")
	@SecurityRequirement(name = "Authentication")
	@Operation(summary = "Get the test execution report")
	@ApiResponse(responseCode = "200", description = "Action successfully executed", content = {
			@Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = TestCaseExecutionReport.class)) })
	@ApiResponse(responseCode = "500", description = "Error executing action", content = {
			@Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = GenericResponse.class)) })	
	public TestCaseExecutionReport getTestExecutionReport(
			@CurrentSecurityContext(expression = "authentication.user") @Parameter(description = "The current user") User user,
			@PathVariable @Parameter(description = "The test session id") String testSessionId) {

		return testExecutionService.getTestCaseExecutionReport(testSessionId);

	}
	
	@GetMapping("/logs/{testSessionId}")
	@SecurityRequirement(name = "Authentication")
	@Operation(summary = "Get the test execution logs")
	@ApiResponse(responseCode = "200", description = "Action successfully executed", content = {
			@Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = TestCaseExecutionLogs.class)) })
	@ApiResponse(responseCode = "500", description = "Error executing action", content = {
			@Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = GenericResponse.class)) })	
	public TestCaseExecutionLogs getTestExecutionLogs(
			@CurrentSecurityContext(expression = "authentication.user") @Parameter(description = "The current user") User user,
			@PathVariable @Parameter(description = "The test session id") String testSessionId) {

		return testExecutionService.getTestCaseExecutionLogs(testSessionId);

	}
}
