package it.eng.simpl.labs.poc.entity;

import java.util.List;

import jakarta.persistence.Column;
import jakarta.persistence.ElementCollection;
import jakarta.persistence.Entity;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import lombok.Getter;
import lombok.Setter;

@Entity
@Getter
@Setter
public class TestCaseExecutionEntity extends BaseTestExecutionEntity {

	@Column(nullable = false)
	private String session;
	
	@ElementCollection
	@Column
	private List<String> logs;
	
	@Column(columnDefinition="TEXT") 
	private String report;

	@Column(name = "test_suite_execution_id", nullable = false)
	private Long testSuiteExecutionId;
	
	@ManyToOne
	@JoinColumn(name = "test_suite_execution_id", insertable = false, updatable = false)
	private TestSuiteExecutionEntity testSuiteExecution;

	@Column(name = "test_case_id", nullable = false)
	private String  testCaseId;
	
	@Column(name = "test_suite_id", nullable = false)
	private String  testSuiteId;
	
	@ManyToOne
	@JoinColumn(name="test_case_id", referencedColumnName="test_case_id", insertable = false, updatable = false)
	@JoinColumn(name="test_suite_id", referencedColumnName="test_suite_id", insertable = false, updatable = false)
	private TestCaseEntity testCase;
	
	@Override
	public String toString() {
		return "TestCaseExecutionEntity [id=" + id + ", status=" + status + ", startDate=" + startDate + ", endDate="
				+ endDate + ", session=" + session + ", testCaseId="
				+ testCaseId + ", testSuiteId=" + testSuiteId + "]";
	}

}
