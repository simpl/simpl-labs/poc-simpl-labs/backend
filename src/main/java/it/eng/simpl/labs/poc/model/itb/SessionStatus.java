package it.eng.simpl.labs.poc.model.itb;

import java.util.ArrayList;
import java.util.List;

import it.eng.simpl.labs.poc.enumeration.ItbResultEnum;
import lombok.Data;

@Data
public class SessionStatus {
	
	private String session;

	private ItbResultEnum result;

	private String startTime;

	private String endTime;

	private String message;

	private List<String> logs = new ArrayList<>();

	private String report;

}
