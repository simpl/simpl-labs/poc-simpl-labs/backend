package it.eng.simpl.labs.poc.model.itb;

import java.util.ArrayList;
import java.util.List;

import lombok.Data;

@Data
public class SessionStartResponse implements ISessionResponse {

	private List<SessionCreationInformation> createdSessions = new ArrayList<>();

}

