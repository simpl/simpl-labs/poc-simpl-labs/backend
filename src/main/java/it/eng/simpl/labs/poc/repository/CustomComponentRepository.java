package it.eng.simpl.labs.poc.repository;

import org.springframework.stereotype.Repository;

import it.eng.simpl.labs.poc.entity.CustomComponentEntity;

@Repository
public interface CustomComponentRepository extends BaseRepository<CustomComponentEntity, Long> {
		
}
