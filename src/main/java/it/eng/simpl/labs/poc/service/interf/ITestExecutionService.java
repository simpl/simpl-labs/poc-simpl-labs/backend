package it.eng.simpl.labs.poc.service.interf;

import java.util.List;

import it.eng.simpl.labs.poc.model.TestCaseExecutionLogs;
import it.eng.simpl.labs.poc.model.TestCaseExecutionReport;
import it.eng.simpl.labs.poc.model.TestSessionStatus;

public interface ITestExecutionService {

	/**
	 * @param componentId
	 * @return
	 */
	public List<TestSessionStatus> getTestHistory(Long componentId);
	
	/**
	 * @param specificationId
	 * @param componentId
	 * @return
	 */
	public TestSessionStatus getTestsExecutionStatus(Long specificationId, Long componentId);
	
	/**
	 * @param sessionId
	 * @return
	 */
	public TestCaseExecutionLogs getTestCaseExecutionLogs(String sessionId);
	
	/**
	 * @param sessionId
	 * @return
	 */
	public TestCaseExecutionReport getTestCaseExecutionReport(String sessionId);

}
