package it.eng.simpl.labs.poc.service.interf;

import java.time.LocalDate;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import it.eng.simpl.labs.poc.model.CurrentConsumption;
import it.eng.simpl.labs.poc.model.HistoricalConsumption;

public interface IMetricsService {
			
    /**
     * @param nodeId
     * @param pageable
     * @return
     */
    public Page<CurrentConsumption> getCurrentMetrics(Long id, Pageable pageable);
    
    /**
     * @param dataspaceId
     * @param startDate
     * @param endDate
     * @return
     */
    public List<HistoricalConsumption> getHistoricalCpuMetrics(Long id, LocalDate startDate, LocalDate endDate);
    
    /**
     * @param nodeId
     * @param startDate
     * @param endDate
     * @return
     */
    public List<HistoricalConsumption> getHistoricalMemoryMetrics(Long id, LocalDate startDate, LocalDate endDate);
    
    /**
     * @param nodeId
     * @param startDate
     * @param endDate
     * @return
     */
    public List<HistoricalConsumption> getHistoricalStorageMetrics(Long id, LocalDate startDate, LocalDate endDate);

}
