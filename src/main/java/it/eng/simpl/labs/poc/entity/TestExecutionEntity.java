package it.eng.simpl.labs.poc.entity;

import java.time.LocalDateTime;
import java.util.List;

import it.eng.simpl.labs.poc.enumeration.TestStatusEnum;
import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Enumerated;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToMany;
import jakarta.validation.constraints.Max;
import jakarta.validation.constraints.Min;
import lombok.Getter;
import lombok.Setter;

@Entity
@Getter
@Setter
public class TestExecutionEntity extends BaseEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column
	@Min(0)
	@Max(9223372036854775807l)
	private Long id;

	@Column
	private LocalDateTime startDate;

	@Column
	private LocalDateTime endDate;
	
	@Column
	private LocalDateTime stopRequestDate;

	@OneToMany(mappedBy = "testExecution", cascade = CascadeType.ALL)
	private List<TestSuiteExecutionEntity> testSuiteExecutionList;

	@Column(name = "custom_component_id")
	private Long customComponentId;

	@ManyToOne
	@JoinColumn(name = "custom_component_id", insertable = false, updatable = false)
	private CustomComponentEntity customComponent;

	@Enumerated
	private TestStatusEnum status;

	@Override
	public String toString() {
		return "TestExecutionEntity [id=" + id + ", status=" + status + ", startDate=" + startDate + ", endDate="
				+ endDate + ", customComponentId=" + customComponentId + "]";
	}

}
