package it.eng.simpl.labs.poc.model;

import java.math.BigDecimal;

import io.swagger.v3.oas.annotations.media.Schema;
import it.eng.simpl.labs.poc.utils.Constants;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class TenantConsumption {

	@Schema(name = "name", description = "The resource name")
	private String name;
	
	@Schema(name = "unitOfMeasurement", description = "The resource unit of measurement")
	private String unitOfMeasurement;

	@Builder.Default
	@Schema(name = "allocatable", description = "The current allocatable resource value")
	private BigDecimal allocatable = Constants.ZERO;
	
	@Builder.Default
	@Schema(name = "allocated", description = "The allocated resource value")
	private BigDecimal allocated = Constants.ZERO; 
	
	@Builder.Default
	@Schema(name = "used", description = "The used resource value")
	private BigDecimal used = Constants.ZERO;
	
	@Schema(name = "date", description = "The resource date of measurement")
	private String date;

}
