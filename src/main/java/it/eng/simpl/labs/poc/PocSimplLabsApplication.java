package it.eng.simpl.labs.poc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;

@SpringBootApplication(exclude = { SecurityAutoConfiguration.class})
public class PocSimplLabsApplication {

	public static void main(String[] args) {
		SpringApplication.run(PocSimplLabsApplication.class, args);
	}

}
