package it.eng.simpl.labs.poc.model;

import java.util.Collection;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.oauth2.jwt.Jwt;
import org.springframework.security.oauth2.server.resource.authentication.JwtAuthenticationToken;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@EqualsAndHashCode
public class JwtUser extends JwtAuthenticationToken{

	/**
	 * A unique serial version identifier
	 */
	private static final long serialVersionUID = 5504357126110056016L;
	
	private User user;
	
	public JwtUser(Jwt jwt, Collection<? extends GrantedAuthority> authorities,User user) {
		super(jwt, authorities);
		this.user=user;
	}

}
