package it.eng.simpl.labs.poc.repository;

import java.util.List;

import org.springframework.stereotype.Repository;

import it.eng.simpl.labs.poc.entity.ComponentEntity;

@Repository
public interface ComponentRepository extends BaseRepository<ComponentEntity, Long> {

	public List<ComponentEntity> findByCategoryIdOrderByTypeAsc(Long categoryId);

}
