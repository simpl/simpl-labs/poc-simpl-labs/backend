package it.eng.simpl.labs.poc.service.interf;

import java.util.List;

import it.eng.simpl.labs.poc.model.CurrentConsumption;

public interface IDataspacesMetricsService extends IMetricsService {
	
    /**
     * @param nodeId
     * @param pageable
     * @return
     */
    public List<CurrentConsumption> getCurrentMetrics(Long id);
			
}
