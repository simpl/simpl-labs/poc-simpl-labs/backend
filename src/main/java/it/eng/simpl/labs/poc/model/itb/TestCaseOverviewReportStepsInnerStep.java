package it.eng.simpl.labs.poc.model.itb;

import lombok.Data;

/**
 * The report for a specific step.
 */
@Data
public class TestCaseOverviewReportStepsInnerStep {

	private String id;

	private TestCaseOverviewReportStepsInnerStepReport report;

}
