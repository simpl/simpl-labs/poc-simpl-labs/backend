package it.eng.simpl.labs.poc.repository;

import org.springframework.stereotype.Repository;

import it.eng.simpl.labs.poc.entity.CategoryEntity;
import it.eng.simpl.labs.poc.enumeration.CategoryTypeEnum;

@Repository
public interface CategoryRepository extends BaseRepository<CategoryEntity, Long>{
	
	public CategoryEntity findByNodeIdAndType(Long nodeId, CategoryTypeEnum type);
	
}