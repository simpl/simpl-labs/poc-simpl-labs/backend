package it.eng.simpl.labs.poc.controller;

import java.util.List;

import org.springframework.http.MediaType;
import org.springframework.security.core.annotation.CurrentSecurityContext;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import it.eng.simpl.labs.poc.model.GenericResponse;
import it.eng.simpl.labs.poc.model.TestCase;
import it.eng.simpl.labs.poc.model.User;
import it.eng.simpl.labs.poc.service.interf.ITestCaseService;
import lombok.RequiredArgsConstructor;

@Tag(name = "Custom Components Test Cases", description = "Custom Components Test Cases APIs")
@RestController
@RequestMapping("/custom-components/test-cases")
@RequiredArgsConstructor
public class TestCaseController {

	private final ITestCaseService testCaseService;
	
	@GetMapping("/{testSuiteId}/{componentId}")
	@SecurityRequirement(name = "Authentication")
	@Operation(summary = "Get all test cases for the test suite with execution status (if any) relative to the component id")
	@ApiResponse(responseCode = "200", description = "Action successfully executed", content = {
			@Content(mediaType = MediaType.APPLICATION_JSON_VALUE, array = @ArraySchema(schema = @Schema(implementation = TestCase.class))) })
	@ApiResponse(responseCode = "500", description = "Error executing action", content = {
			@Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = GenericResponse.class)) })	
	public List<TestCase> getTestCasesByTestSuiteId(
			@CurrentSecurityContext(expression = "authentication.user") @Parameter(description = "The current user") User user,
			@PathVariable @Parameter(description = "The test suite id") String testSuiteId,
			@PathVariable @Parameter(description = "The component id") Long componentId) {

		return testCaseService.findByTestSuiteId(testSuiteId, componentId);

	}
}
