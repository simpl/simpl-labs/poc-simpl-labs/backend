package it.eng.simpl.labs.poc.model;

import org.springframework.data.domain.Page;
import org.springframework.data.web.PagedModel;

/**
 * 
 */
public class CurrentConsumptionWrapper extends PagedModel<CurrentConsumption> {
	
	/**
	 * @param page
	 */
	public CurrentConsumptionWrapper(Page<CurrentConsumption> page) {
		super(page);
	}	

}
