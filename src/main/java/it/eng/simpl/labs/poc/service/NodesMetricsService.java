package it.eng.simpl.labs.poc.service;

import java.time.LocalDate;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.eng.simpl.labs.poc.entity.CurrentMetricsEntity;
import it.eng.simpl.labs.poc.model.CurrentConsumption;
import it.eng.simpl.labs.poc.model.HistoricalConsumption;
import it.eng.simpl.labs.poc.repository.CurrentMetricsRepository;
import it.eng.simpl.labs.poc.repository.HistoricalMetricsRepository;
import it.eng.simpl.labs.poc.service.interf.INodesMetricsService;
import lombok.RequiredArgsConstructor;

@Service
@Transactional
@RequiredArgsConstructor
public class NodesMetricsService extends AbstractMetricsService implements INodesMetricsService {

	private final HistoricalMetricsRepository historicalMetricsRepository;
	
	private final CurrentMetricsRepository currentMetricsRepository;
		
	@Override
    public Page<CurrentConsumption> getCurrentMetrics(Long id, Pageable pageable) {
		        
        List<CurrentMetricsEntity> currentMetricsEntityListGlobal = currentMetricsRepository.findByNodeId(id);
        
        Page<CurrentMetricsEntity> currentMetricsEntityPage = currentMetricsRepository.findByNodeId(id, pageable);
        
        return getCurrentMetrics(currentMetricsEntityListGlobal, currentMetricsEntityPage);

    }
	
	@Override
	public List<HistoricalConsumption> getHistoricalCpuMetrics(Long id, LocalDate startDate, LocalDate endDate) {
		List<Object[]> list = historicalMetricsRepository.getNodesHistoricalMetrics(startDate, endDate, id);
		return getHistoricalCpuMetrics(list);
	}
	
	@Override
	public List<HistoricalConsumption> getHistoricalMemoryMetrics(Long id, LocalDate startDate, LocalDate endDate) {
		List<Object[]> list = historicalMetricsRepository.getNodesHistoricalMetrics(startDate, endDate, id);
		return getHistoricalMemoryMetrics(list);
	}

	@Override
	public List<HistoricalConsumption> getHistoricalStorageMetrics(Long id, LocalDate startDate, LocalDate endDate) {
		List<Object[]> list = historicalMetricsRepository.getNodesHistoricalMetrics(startDate, endDate, id);
		return getHistoricalStorageMetrics(list);
	}
		
}
