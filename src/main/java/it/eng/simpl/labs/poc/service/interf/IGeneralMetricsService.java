package it.eng.simpl.labs.poc.service.interf;

import java.time.LocalDate;
import java.util.List;

import it.eng.simpl.labs.poc.model.CurrentConsumption;
import it.eng.simpl.labs.poc.model.HistoricalConsumption;
import it.eng.simpl.labs.poc.model.TenantConsumption;
import it.eng.simpl.labs.poc.model.User;

public interface IGeneralMetricsService {
		
    /**
     * @param user
     * @return
     */
    public List<CurrentConsumption> getCurrentMetrics(User user);
    
    /**
     * @param user
     * @return
     */
	public TenantConsumption getCurrentCpuMetrics(User user);
	
    /**
     * @param user
     * @return
     */
	public TenantConsumption getCurrentMemoryMetrics(User user);
	
    /**
     * @param user
     * @return
     */
	public TenantConsumption getCurrentStorageMetrics(User user);
	
	/**
	 * @param startDate
	 * @param endDate
	 * @param user
	 * @return
	 */
	public List<HistoricalConsumption> getHistoricalCpuMetrics(LocalDate startDate, LocalDate endDate, User user);
	
	/**
	 * @param startDate
	 * @param endDate
	 * @param user
	 * @return
	 */
	public List<HistoricalConsumption> getHistoricalMemoryMetrics(LocalDate startDate, LocalDate endDate, User user);
	
	/**
	 * @param startDate
	 * @param endDate
	 * @param user
	 * @return
	 */
	public List<HistoricalConsumption> getHistoricalStorageMetrics(LocalDate startDate, LocalDate endDate, User user);

}
