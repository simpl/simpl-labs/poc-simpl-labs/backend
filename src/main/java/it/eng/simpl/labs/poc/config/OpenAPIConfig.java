package it.eng.simpl.labs.poc.config;

import org.springframework.context.annotation.Configuration;

import io.swagger.v3.oas.annotations.enums.SecuritySchemeIn;
import io.swagger.v3.oas.annotations.enums.SecuritySchemeType;
import io.swagger.v3.oas.annotations.security.SecurityScheme;

@Configuration
@SecurityScheme(name = "Authentication", 
			    description = "JWT token", 
			    scheme = "Bearer", 
			    type = SecuritySchemeType.HTTP, 
			    in = SecuritySchemeIn.HEADER)
public class OpenAPIConfig {
}