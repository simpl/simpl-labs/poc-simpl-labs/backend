package it.eng.simpl.labs.poc.controller;

import java.util.List;

import org.springframework.http.MediaType;
import org.springframework.security.core.annotation.CurrentSecurityContext;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import it.eng.simpl.labs.poc.model.Dataspace;
import it.eng.simpl.labs.poc.model.User;
import it.eng.simpl.labs.poc.service.interf.IDataspaceService;
import lombok.RequiredArgsConstructor;

@Tag(name = "Data Spaces", description = "Data Spaces management APIs")
@RestController
@RequestMapping("/dataspaces")
@RequiredArgsConstructor
public class DataspaceController {

	private final IDataspaceService dataspaceService;
	
	@GetMapping("/{id}")
	@SecurityRequirement(name = "Authentication")
	@Operation(summary = "Get Data Space by id", description = "Get Data Space by id")
	@ApiResponse(responseCode = "200", description = "List of Data Spaces for current user", content = {
			@Content(mediaType = MediaType.APPLICATION_JSON_VALUE) })
	public Dataspace findByid(
			@PathVariable @Parameter(description = "The data space id") Long id,
			@CurrentSecurityContext(expression = "authentication.user") User user) {
		return dataspaceService.findById(id);
	}

	@SecurityRequirement(name = "Authentication")
	@GetMapping
	@Operation(summary = "Get Data Spaces for current user", description = "Get Data Spaces for current user")
	@ApiResponse(responseCode = "200", description = "List of Data Spaces for current user", content = {
			@Content(mediaType = MediaType.APPLICATION_JSON_VALUE) })
	public List<Dataspace> findAll(@CurrentSecurityContext(expression = "authentication.user") User user) {
		return dataspaceService.findAll(user);
	}

	@SecurityRequirement(name = "Authentication")
	@PostMapping
	@Operation(summary = "Create Data Space", description = "Create Data Space")
	@ApiResponse(responseCode = "200", description = "Data Space created", content = {
			@Content(mediaType = MediaType.APPLICATION_JSON_VALUE) })
	public Dataspace save(@RequestBody Dataspace dataspace,
			@Parameter(hidden = true) @CurrentSecurityContext(expression = "authentication.user") User user) {
		return dataspaceService.save(dataspace, user);
	}
	
	@SecurityRequirement(name = "Authentication")
	@DeleteMapping(value = "/{dataspaceId}")
	@Operation(summary = "Delete Data Space", description = "Delete Data Space")
	@ApiResponse(responseCode = "200", description = "Data Space Deletion requested", content = {
			@Content(mediaType = MediaType.APPLICATION_JSON_VALUE) })
	public Dataspace delete(
			@Parameter @PathVariable("dataspaceId") Long dataspaceId,

			@Parameter(hidden = true) @CurrentSecurityContext(expression = "authentication.user") User user) {
		return dataspaceService.delete(dataspaceId, user);
	}

}
