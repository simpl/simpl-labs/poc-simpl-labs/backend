package it.eng.simpl.labs.poc.service;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.RequestEntity;
import org.springframework.http.RequestEntity.BodyBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import it.eng.simpl.labs.poc.model.itb.ISessionRequest;
import it.eng.simpl.labs.poc.model.itb.ISessionResponse;
import it.eng.simpl.labs.poc.model.itb.SessionStartResponse;
import it.eng.simpl.labs.poc.model.itb.SessionStatusResponse;
import it.eng.simpl.labs.poc.model.itb.SessionStopResponse;
import it.eng.simpl.labs.poc.service.interf.IItbClientService;
import it.eng.simpl.labs.poc.utils.Constants;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Component
@RequiredArgsConstructor
@Slf4j
public class ItbClientService implements IItbClientService {

	@Value("${itb.base.path}")
	protected String basePath;

	@Value("${itb.organisation.api.key}")
	protected String organisationApiKey;

	private final RestTemplate restTemplate;
	
	@Override
	public ISessionResponse startTestSession(ISessionRequest sessionRequest, String finalUri) {

		RequestEntity<ISessionRequest> requestEntity = buildSessionRequest(sessionRequest, finalUri);
		
		ResponseEntity<SessionStartResponse> responseEntity = restTemplate.exchange(
				requestEntity,
				new ParameterizedTypeReference<SessionStartResponse>() {});

		return buildSessionResponse(responseEntity);

	}
	
	@Override
	public ISessionResponse stopTestSession(ISessionRequest sessionRequest, String finalUri) {

		RequestEntity<ISessionRequest> requestEntity = buildSessionRequest(sessionRequest, finalUri);

		ResponseEntity<SessionStopResponse> responseEntity = restTemplate.exchange(
				requestEntity,
				new ParameterizedTypeReference<SessionStopResponse>() {});

		return buildSessionResponse(responseEntity);

	}
	
	@Override
	public ISessionResponse getSessionStatus(ISessionRequest sessionRequest, String finalUri) {

		RequestEntity<ISessionRequest> requestEntity = buildSessionRequest(sessionRequest, finalUri);

		ResponseEntity<SessionStatusResponse> responseEntity = restTemplate.exchange(
				requestEntity,
				new ParameterizedTypeReference<SessionStatusResponse>() {});

		return buildSessionResponse(responseEntity);

	}

	private ISessionResponse buildSessionResponse(ResponseEntity<?> responseEntity) {
		
		if (responseEntity.getStatusCode().is2xxSuccessful()) {
			
			log.debug("Response : {}", responseEntity.getBody());

			return (ISessionResponse) responseEntity.getBody();
		} else {
			throw new RestClientException("API returned " + responseEntity.getStatusCode()
			+ " and it wasn't handled by the RestTemplate error handler");
		}
	}

	private RequestEntity<ISessionRequest> buildSessionRequest(ISessionRequest sessionRequest, String finalUri) {
		
		BodyBuilder requestBuilder = RequestEntity.method(
				HttpMethod.POST, 
				UriComponentsBuilder.fromHttpUrl(basePath).toUriString() + finalUri);
		requestBuilder.accept(MediaType.APPLICATION_JSON);
		requestBuilder.contentType(MediaType.APPLICATION_JSON);
		requestBuilder.header(Constants.ITB_API_KEY, organisationApiKey);
		requestBuilder.header(Constants.USER_AGENT, Constants.JAVA_SDK);

		RequestEntity<ISessionRequest> requestEntity = requestBuilder.body(sessionRequest);
		
		log.debug("Request : {}", requestEntity.toString());

		return requestEntity;
	}

}