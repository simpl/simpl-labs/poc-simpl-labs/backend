package it.eng.simpl.labs.poc.model;

import java.util.List;

import io.swagger.v3.oas.annotations.media.Schema;
import it.eng.simpl.labs.poc.model.itb.InputMapping;
import lombok.Builder;
import lombok.Data;

/**
 * Information on the test session(s) to start.
 */
@Data
@Builder
public class TestSessionStartRequest {

	@Schema(name = "forceSequentialExecution", description = "The force sequential execution flag")
	private Boolean forceSequentialExecution;

	@Schema(name = "testSuite", description = "The test suites list")
	private List<String> testSuite;

	@Schema(name = "testCase", description = "The test cases list")
	private List<String> testCase;

	@Schema(name = "inputMapping", description = "The input mapping object")
	private List<InputMapping> inputMapping;
	
	@Schema(name = "customComponentId", description = "The custom component identification")
	private Long customComponentId;
	
	private String system;

}

