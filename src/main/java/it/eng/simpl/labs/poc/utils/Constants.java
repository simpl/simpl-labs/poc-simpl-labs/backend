package it.eng.simpl.labs.poc.utils;

import java.math.BigDecimal;

public class Constants {
	
	private Constants() {
		throw new IllegalStateException("Utility class");
	}	
	
	public static final BigDecimal ZERO = new BigDecimal(0);
	
	public static final BigDecimal ONE_HUNDRED = new BigDecimal(100);
	
	public static final String UNDEFINED_LOWERCASE = "Undefined";
	
	public static final String GB_IN_BYTES = "1073741824";
	
	public static final Integer DEFAULT_PAGINATION_OFFSET = 0;
	
	public static final Integer DEFAULT_PAGE_SIZE = 10;
	
	public static final String CPU_LOWERCASE = "cpu";
	
	public static final String CORES_LOWERCASE = "cores";
	
	public static final String CPU = "CPU";
	
	public static final String GIGABYTES = "GB";
	
	public static final String MEMORY = "Memory";
	
	public static final String MEMORY_LOWERCASE = "memory";
	
	public static final String STORAGE = "Storage";
	
	public static final String STORAGE_LOWERCASE = "storage";
	
	public static final String JAVA_SDK = "Java-SDK";

	public static final String USER_AGENT = "User-Agent";

	public static final String ITB_API_KEY = "ITB_API_KEY";
	    
	public static final int CPU_INDEX = 1;
	
	public static final int MEMORY_INDEX = 3;
	
	public static final int STORAGE_INDEX = 5;
}
