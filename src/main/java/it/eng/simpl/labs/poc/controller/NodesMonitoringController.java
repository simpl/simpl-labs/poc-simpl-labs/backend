package it.eng.simpl.labs.poc.controller;

import java.time.LocalDate;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.MediaType;
import org.springframework.security.core.annotation.CurrentSecurityContext;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import it.eng.simpl.labs.poc.model.CurrentConsumption;
import it.eng.simpl.labs.poc.model.CurrentConsumptionWrapper;
import it.eng.simpl.labs.poc.model.GenericResponse;
import it.eng.simpl.labs.poc.model.HistoricalConsumption;
import it.eng.simpl.labs.poc.model.User;
import it.eng.simpl.labs.poc.service.interf.INodesMetricsService;
import it.eng.simpl.labs.poc.utils.GenericUtils;
import lombok.RequiredArgsConstructor;

@Tag(name = "System Monitoring", description = "System Monitoring management APIs")
@RestController
@RequestMapping("/system-monitoring")
@RequiredArgsConstructor
public class NodesMonitoringController {
	
	private final INodesMetricsService nodesMetricsService;
			
	@GetMapping("/node/current/{nodeId}")
	@SecurityRequirement(name = "Authentication")
	@Operation(summary = "Get the node current resources usage")
	@ApiResponse(responseCode = "200", description = "Action successfully executed", content = {
			@Content(mediaType = MediaType.APPLICATION_JSON_VALUE, array = @ArraySchema(schema = @Schema(implementation = CurrentConsumptionWrapper.class))) })
	@ApiResponse(responseCode = "500", description = "Error executing action", content = {
			@Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = GenericResponse.class)) })
	public Page<CurrentConsumption> getCurrentMetrics(
			@Parameter @PathVariable("nodeId") Long nodeId,
			@RequestParam(value = "pageNumber", required = true) Integer pageNumber,
            @RequestParam(value = "pageSize", required = true) Integer pageSize,
            @RequestParam(value = "sortBy", required = false) String sortBy,
			@CurrentSecurityContext(expression = "authentication.user") @Parameter(description = "The current user") User user ) {

		return nodesMetricsService.getCurrentMetrics(nodeId, GenericUtils.getPageRequest(pageNumber, pageSize, sortBy));
	}
	
	@GetMapping("/node/historical/cpu/{nodeId}/{startDate}/{endDate}")
	@SecurityRequirement(name = "Authentication")
	@Operation(summary = "Get the node historical CPU usage")
	@ApiResponse(responseCode = "200", description = "Action successfully executed", content = {
			@Content(mediaType = MediaType.APPLICATION_JSON_VALUE, array = @ArraySchema(schema = @Schema(implementation = HistoricalConsumption.class))) })
	@ApiResponse(responseCode = "500", description = "Error executing action", content = {
			@Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = GenericResponse.class)) })
	public List<HistoricalConsumption> getHistoricalCpuMetrics(
			@Parameter @PathVariable("nodeId") Long nodeId,
			@Parameter @PathVariable("startDate") @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate startDate,
			@Parameter @PathVariable("endDate") @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate endDate,
			@CurrentSecurityContext(expression = "authentication.user") @Parameter(description = "The current user") User user ) {

		return nodesMetricsService.getHistoricalCpuMetrics(nodeId, startDate, endDate);

	}
	
	@GetMapping("/node/historical/memory/{nodeId}/{startDate}/{endDate}")
	@SecurityRequirement(name = "Authentication")
	@Operation(summary = "Get the node historical Memory usage")
	@ApiResponse(responseCode = "200", description = "Action successfully executed", content = {
			@Content(mediaType = MediaType.APPLICATION_JSON_VALUE, array = @ArraySchema(schema = @Schema(implementation = HistoricalConsumption.class))) })
	@ApiResponse(responseCode = "500", description = "Error executing action", content = {
			@Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = GenericResponse.class)) })
	public List<HistoricalConsumption> getHistoricalMemoryMetrics(
			@Parameter @PathVariable("nodeId") Long nodeId,
			@Parameter @PathVariable("startDate") @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate startDate,
			@Parameter @PathVariable("endDate") @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate endDate,
			@CurrentSecurityContext(expression = "authentication.user") @Parameter(description = "The current user") User user ) {

		return nodesMetricsService.getHistoricalMemoryMetrics(nodeId, startDate, endDate);

	}
	
	@GetMapping("/node/historical/storage/{nodeId}/{startDate}/{endDate}")
	@SecurityRequirement(name = "Authentication")
	@Operation(summary = "Get the node historical Storage usage")
	@ApiResponse(responseCode = "200", description = "Action successfully executed", content = {
			@Content(mediaType = MediaType.APPLICATION_JSON_VALUE, array = @ArraySchema(schema = @Schema(implementation = HistoricalConsumption.class))) })
	@ApiResponse(responseCode = "500", description = "Error executing action", content = {
			@Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = GenericResponse.class)) })
	public List<HistoricalConsumption> getHistoricalStorageMetrics(
			@Parameter @PathVariable("nodeId") Long nodeId,
			@Parameter @PathVariable("startDate") @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate startDate,
			@Parameter @PathVariable("endDate") @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate endDate,
			@CurrentSecurityContext(expression = "authentication.user") @Parameter(description = "The current user") User user ) {

		return nodesMetricsService.getHistoricalStorageMetrics(nodeId, startDate, endDate);

	}

}
