package it.eng.simpl.labs.poc.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.eng.simpl.labs.poc.entity.TestCaseEntity;
import it.eng.simpl.labs.poc.entity.TestCaseExecutionEntity;
import it.eng.simpl.labs.poc.mapper.TestCaseMapper;
import it.eng.simpl.labs.poc.model.TestCase;
import it.eng.simpl.labs.poc.repository.TestCaseExecutionRepository;
import it.eng.simpl.labs.poc.repository.TestCaseRepository;
import it.eng.simpl.labs.poc.service.interf.ITestCaseService;
import it.eng.simpl.labs.poc.utils.Constants;
import lombok.RequiredArgsConstructor;

@Service
@Transactional
@RequiredArgsConstructor
public class TestCaseService implements ITestCaseService {

	private final TestCaseRepository testCaseRepository;

	private final TestCaseExecutionRepository testCaseExecutionRepository;

	private final TestCaseMapper testCaseMapper;

	@Override
	public List<TestCase> findByTestSuiteId(String testSuiteId, Long componentId) {

		List<TestCase> testCaselist = new ArrayList<>();

		// Get all test cases associated to the provided test suite

		List<TestCaseEntity> testCaseEntityList = testCaseRepository.findByTestSuiteId(testSuiteId);

		for (TestCaseEntity testCaseEntity : testCaseEntityList) {

			TestCase testCase = testCaseMapper.createFromEntity(testCaseEntity);
			testCase.setTestSuiteId(testSuiteId);
			
			// For each test case, and for the given component, check if there is an ongoing/completed session
			
			List<TestCaseExecutionEntity> testCaseExecutionEntityList = testCaseExecutionRepository.findLastExecution(testCaseEntity.getId().getTestCaseId(), componentId);
			
			if (testCaseExecutionEntityList != null && !testCaseExecutionEntityList.isEmpty()) {
				// If present set the TC status with the last execution status
				testCase.setStatus(testCaseExecutionEntityList.get(0).getStatus() != null ? testCaseExecutionEntityList.get(0).getStatus().getValue() : Constants.UNDEFINED_LOWERCASE);
				testCase.setStartDate(testCaseExecutionEntityList.get(0).getStartDate());
				testCase.setEndDate(testCaseExecutionEntityList.get(0).getEndDate());
			}

			testCaselist.add(testCase);
		}

		return testCaselist;

	}

}
