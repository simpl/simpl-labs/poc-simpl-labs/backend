package it.eng.simpl.labs.poc.controller;

import java.util.List;

import org.springframework.http.MediaType;
import org.springframework.security.core.annotation.CurrentSecurityContext;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import it.eng.simpl.labs.poc.model.CustomComponent;
import it.eng.simpl.labs.poc.model.Dataspace;
import it.eng.simpl.labs.poc.model.GenericResponse;
import it.eng.simpl.labs.poc.model.User;
import it.eng.simpl.labs.poc.service.interf.ICustomComponentService;
import lombok.RequiredArgsConstructor;

@Tag(name = "Custom Components", description = "Custom Components management APIs")
@RestController
@RequestMapping("/custom-components")
@RequiredArgsConstructor
public class CustomComponentController {

	private final ICustomComponentService customComponentService;
		
	@GetMapping
	@SecurityRequirement(name = "Authentication")
	@Operation(summary = "Get all components")
	@ApiResponse(responseCode = "200", description = "Action successfully executed", content = {
			@Content(mediaType = MediaType.APPLICATION_JSON_VALUE, array = @ArraySchema(schema = @Schema(implementation = CustomComponent.class))) })
	@ApiResponse(responseCode = "500", description = "Error executing action", content = {
			@Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = GenericResponse.class)) })
	public List<CustomComponent> getAllCustomComponents(
			@CurrentSecurityContext(expression = "authentication.user") @Parameter(description = "The current user") User user) {

		return customComponentService.findAll();

	}
	
	@GetMapping("/dataspaces/{componentId}")
	@SecurityRequirement(name = "Authentication")
	@Operation(summary = "Gets all Dataspaces that contain the component")
	@ApiResponse(responseCode = "200", description = "Action successfully executed", content = {
			@Content(mediaType = MediaType.APPLICATION_JSON_VALUE, array = @ArraySchema(schema = @Schema(implementation = Dataspace.class))) })
	@ApiResponse(responseCode = "500", description = "Error executing action", content = {
			@Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = GenericResponse.class)) })	
	public List<Dataspace> getDataSpaces(
			@CurrentSecurityContext(expression = "authentication.user") @Parameter(description = "The current user") User user,
			@PathVariable @Parameter(description = "The component id") Long componentId) {

		return customComponentService.getDataspaces(componentId, user);

	}
	
}
