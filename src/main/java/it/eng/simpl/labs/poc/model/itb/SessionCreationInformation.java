package it.eng.simpl.labs.poc.model.itb;

import lombok.Data;

@Data
public class SessionCreationInformation {
	
	private String testSuite;

	private String testCase;

	private String session;

}

