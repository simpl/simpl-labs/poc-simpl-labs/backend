package it.eng.simpl.labs.poc.repository;

import org.springframework.stereotype.Repository;

import it.eng.simpl.labs.poc.entity.SpecificationEntity;

@Repository
public interface SpecificationRepository extends BaseRepository<SpecificationEntity, Long> {
	
}
