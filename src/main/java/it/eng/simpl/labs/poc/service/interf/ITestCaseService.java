package it.eng.simpl.labs.poc.service.interf;

import java.util.List;

import it.eng.simpl.labs.poc.model.TestCase;

public interface ITestCaseService {

	/**
	 * @param testSuiteId
	 * @param componentId
	 * @return
	 */
	public List<TestCase> findByTestSuiteId(String testSuiteId, Long componentId);

}
