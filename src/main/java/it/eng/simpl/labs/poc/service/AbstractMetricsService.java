package it.eng.simpl.labs.poc.service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;

import it.eng.simpl.labs.poc.entity.CurrentMetricsEntity;
import it.eng.simpl.labs.poc.model.CurrentConsumption;
import it.eng.simpl.labs.poc.model.HistoricalConsumption;
import it.eng.simpl.labs.poc.utils.Constants;
import it.eng.simpl.labs.poc.utils.GenericUtils;

public abstract class AbstractMetricsService {
	
	protected List<CurrentConsumption> getCurrentMetrics(List<CurrentMetricsEntity> currentMetricsEntityListGlobal) {

		List<CurrentConsumption> currentDataspacesConsumpionsList = new ArrayList<>();

		for(CurrentMetricsEntity currentMetricsEntity : currentMetricsEntityListGlobal) {

			CurrentConsumption currentDataspacesConsumpions = CurrentConsumption.builder()
					.componentId(currentMetricsEntity.getComponentId())
					.componentName(currentMetricsEntity.getComponent().getType().name())
					.categoryId(currentMetricsEntity.getComponent().getCategory().getId())
					.categoryName(currentMetricsEntity.getComponent().getCategory().getType().name())
					.nodeId(currentMetricsEntity.getComponent().getCategory().getNode().getId())
					.nodeType(currentMetricsEntity.getComponent().getCategory().getNode().getType().name())
					.nodeName(currentMetricsEntity.getComponent().getCategory().getNode().getName())
					.id(currentMetricsEntity.getDataspaceId())
					.name(currentMetricsEntity.getComponent().getCategory().getNode().getDataspace().getName())
					.status(currentMetricsEntity.getComponent().getCategory().getNode().getDataspace().getStatus().name())
					.build();

			currentDataspacesConsumpionsList.add(currentDataspacesConsumpions);
		}

		return currentDataspacesConsumpionsList;
	}
		
    protected Page<CurrentConsumption> getCurrentMetrics(List<CurrentMetricsEntity> currentMetricsEntityListGlobal, Page<CurrentMetricsEntity> currentMetricsEntityPage) {
		
        List<CurrentConsumption> currentDataspacesConsumpionsList = new ArrayList<>();
                
        BigDecimal cpuAllocated = Constants.ZERO;
        BigDecimal memoryAllocated = Constants.ZERO;
        BigDecimal storageAllocated = Constants.ZERO;
        for(CurrentMetricsEntity currentMetricsEntity : currentMetricsEntityListGlobal ) {
            cpuAllocated = cpuAllocated.add(currentMetricsEntity.getCpuAllocated());
            memoryAllocated = memoryAllocated.add(currentMetricsEntity.getMemoryAllocated());
            storageAllocated = storageAllocated.add(currentMetricsEntity.getStorageAllocated());
        }
                
        for(CurrentMetricsEntity currentMetricsEntity : currentMetricsEntityPage) {
            
            CurrentConsumption currentDataspacesConsumpions = CurrentConsumption.builder()
                    .componentId(currentMetricsEntity.getComponentId())
                    .componentName(currentMetricsEntity.getComponent().getType().name())
                    .categoryId(currentMetricsEntity.getComponent().getCategory().getId())
                    .categoryName(currentMetricsEntity.getComponent().getCategory().getType().name())
                    .nodeId(currentMetricsEntity.getComponent().getCategory().getNode().getId())
                    .nodeType(currentMetricsEntity.getComponent().getCategory().getNode().getType().name())
                    .nodeName(currentMetricsEntity.getComponent().getCategory().getNode().getName())
                    .id(currentMetricsEntity.getDataspaceId())
                    .name(currentMetricsEntity.getComponent().getCategory().getNode().getDataspace().getName())
                    .status(currentMetricsEntity.getComponent().getCategory().getNode().getDataspace().getStatus().name())
                    .cpuUsed(currentMetricsEntity.getCpuUsed())
                    .cpuAllocated(cpuAllocated)
                    .memoryUsed(GenericUtils.getGigaBytesFromBytes(currentMetricsEntity.getMemoryUsed()))
                    .memoryAllocated(GenericUtils.getGigaBytesFromBytes(memoryAllocated))
                    .storageUsed(currentMetricsEntity.getStorageUsed())
                    .storageAllocated(storageAllocated)
                    .build();
            
            currentDataspacesConsumpionsList.add(currentDataspacesConsumpions);
        }
                
        return new PageImpl<>(currentDataspacesConsumpionsList, currentMetricsEntityPage.getPageable(), currentMetricsEntityPage.getTotalElements());
    }
	
    protected List<HistoricalConsumption> getHistoricalCpuMetrics(List<Object[]> list) {
		return GenericUtils.getModelFromObjectsList(list, Constants.CPU_INDEX);
	}
	
    protected List<HistoricalConsumption> getHistoricalMemoryMetrics(List<Object[]> list) {
		return GenericUtils.getModelFromObjectsList(list, Constants.MEMORY_INDEX);
	}

    protected List<HistoricalConsumption> getHistoricalStorageMetrics(List<Object[]> list) {
		return GenericUtils.getModelFromObjectsList(list, Constants.STORAGE_INDEX);
	}
		
}
