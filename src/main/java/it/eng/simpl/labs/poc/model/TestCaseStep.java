package it.eng.simpl.labs.poc.model;

import java.time.LocalDateTime;

import io.swagger.v3.oas.annotations.media.Schema;
import it.eng.simpl.labs.poc.utils.Constants;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class TestCaseStep {

	@Schema(name = "id", description = "The test case step id")
	private String id;
	
	@Schema(name = "progressiveId", description = "The test case step progressive id")
	private String progressiveId;

	@Schema(name = "description", description = "The test case step description")
	private String description;
	
	@Builder.Default
	@Schema(name = "result", description = "The test case step result")
	private String result = Constants.UNDEFINED_LOWERCASE;
	
	@Schema(name = "date", description = "The test case step date")
	private LocalDateTime date;

}
