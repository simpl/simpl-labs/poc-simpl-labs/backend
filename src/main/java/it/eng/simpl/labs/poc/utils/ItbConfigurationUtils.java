package it.eng.simpl.labs.poc.utils;

import java.io.StringReader;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import it.eng.simpl.labs.poc.exception.GenericException;

/**
 * 
 */
public class ItbConfigurationUtils {
	
	private ItbConfigurationUtils() {
		throw new IllegalStateException("Utility class");
	}

	/**
	 * @param xml
	 * @param stepId
	 * @return
	 */
	public static String getTestCaseStepResult(String xml, String stepId) {

		try {
			DocumentBuilderFactory factory=DocumentBuilderFactory.newInstance();
            factory.setFeature("http://apache.org/xml/features/disallow-doctype-decl", true);
			DocumentBuilder builder = factory.newDocumentBuilder();
			InputSource is = new InputSource(new StringReader(xml));
			Document doc = builder.parse(is);
			doc.getDocumentElement().normalize();

			XPath xPath = XPathFactory.newInstance().newXPath();
			NodeList resultList = (NodeList) xPath.compile("/TestCaseOverviewReport/steps/*/report/result").evaluate(doc, XPathConstants.NODESET);

			for (int i = 0; i < resultList.getLength(); i++) {

				if ("report".equals(resultList.item(i).getParentNode().getNodeName())
						&& "step".equals(resultList.item(i).getParentNode().getParentNode().getNodeName())) {

					Element actorElement = (Element) resultList.item(i).getParentNode().getParentNode();
					String id = actorElement.getAttribute("id");

					if (stepId.equals(id)) {
						return resultList.item(i).getTextContent();
					}
				}
			}
		} catch (Exception e) {
			throw new GenericException("Incorrect ITB configuration");
		}

		return null;
	}


}
