package it.eng.simpl.labs.poc.model;

import java.nio.file.Paths;

import org.springframework.stereotype.Component;

import com.marcnuri.helm.Helm;
import com.marcnuri.helm.UninstallCommand;

@Component
public class UninstallCommandWrapper {
	
	private UninstallCommand uninstallCommand;
	
	public UninstallCommand init(String releaseName) {
		
		this.uninstallCommand  = Helm.uninstall(releaseName);

		return uninstallCommand;
	}
	
	public UninstallCommand set(String kubeconfigFile, String namespace) {
		
		uninstallCommand.ignoreNotFound().withKubeConfig(Paths.get((kubeconfigFile))).withNamespace(namespace);
		
		return uninstallCommand;

	}
	
	public UninstallCommand uninstall() {
		
		uninstallCommand.call();
		
		return uninstallCommand;

	}
	
}
