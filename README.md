# Simpl-Labs PoC - backend

## Project Description

This project is part of the PoC developed for Simpl-Labs, required for the Simpl-Labs feasibility project.

This project serves as the core engine responsible for managing and processing requests from the frontend and interacting with the underlying infrastructure. It acts as the intermediary between the user interface and the system’s internal functionalities, ensuring seamless communication, data management, and task execution. 

## Features 

The backend exposes several essential functionalities for the PoC. These include user authentication and authorization, ensuring that only validated users can access specific features. It also handles the creation, management, and configuration of data spaces, allowing users to define and modify their environments. Additionally, the backend supports conformance testing of custom components, providing APIs to validate these components against predefined standards. Last key feature include monitoring about different metrics for the Simpl-Labs user.

## Technology 

It is a Java 21 project based on the Spring Boot framework version 3.3.3.